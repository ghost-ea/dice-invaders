#include "ServiceLocator.hpp"

#include "DiceInvaders.h"
#include "EngineManager.hpp"
#include "ResourceManager.hpp"
#include "RenderManager.hpp"
#include "LevelManager.hpp"
#include "GameManager.hpp"
#include "CollisionManager.hpp"

namespace ea
{
    ServiceLocator::ServiceLocator()
    {
    }

    ServiceLocator::~ServiceLocator()
    {
    }

    ServiceLocator& ServiceLocator::get()
    {
        static ServiceLocator locator;
        return locator;
    }

    bool ServiceLocator::startUp(IDiceInvaders* engine)
    {
        // Startup managers in correct order
        _engineManager = new EngineManager();
        _resourceManager = new ResourceManager();
        _levelManager = new LevelManager();
        _gameManager = new GameManager();
        _renderManager = new RenderManager();
        _collisionManager = new CollisionManager();

        _engineManager->startUp(engine);
        _resourceManager->startUp();
        _levelManager->startUp();
        _gameManager->startUp();
        _renderManager->startUp();
        _collisionManager->startUp();

        return true;
    }

    void ServiceLocator::shutDown()
    {
        _collisionManager->shutDown();
        _renderManager->shutDown();
        _levelManager->shutDown();
        _resourceManager->shutDown();
        _engineManager->shutDown();

        SAFE_DELETE(_collisionManager);
        SAFE_DELETE(_renderManager);
        SAFE_DELETE(_gameManager);
        SAFE_DELETE(_levelManager);
        SAFE_DELETE(_resourceManager);
        SAFE_DELETE(_engineManager);
    }
}

#ifndef __DiceInvaders__ServiceLocator__hpp__
#define __DiceInvaders__ServiceLocator__hpp__

#include "ServiceLocator.fwd.hpp"

#include "EngineManager.fwd.hpp"
#include "ResourceManager.fwd.hpp"
#include "RenderManager.fwd.hpp"
#include "LevelManager.fwd.hpp"
#include "GameManager.fwd.hpp"
#include "CollisionManager.fwd.hpp"

#include "Types.hpp"

namespace ea
{
    class ServiceLocator
    {
    private:
        EngineManager* _engineManager = nullptr;
        ResourceManager* _resourceManager = nullptr;
        RenderManager* _renderManager = nullptr;
        LevelManager* _levelManager = nullptr;
        GameManager* _gameManager = nullptr;
        CollisionManager* _collisionManager = nullptr;

    private:
        ServiceLocator();

    public:
        static ServiceLocator& get();
        virtual ~ServiceLocator();

        bool startUp(IDiceInvaders* system);
        void shutDown();

        EngineManager& getEngineManager()
        {
            return *_engineManager;
        }

        ResourceManager& getResourceManager()
        {
            return *_resourceManager;
        }

        RenderManager& getRenderManager()
        {
            return *_renderManager;
        }

        LevelManager& getLevelManager()
        {
            return *_levelManager;
        }

        GameManager& getGameManager()
        {
            return *_gameManager;
        }

        CollisionManager& getCollisionManager()
        {
            return *_collisionManager;
        }
    };
}

#endif //defined(__DiceInvaders__ServiceLocator__hpp__)


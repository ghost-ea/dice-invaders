#ifndef __DICE_INVADERS_LIB__
#define __DICE_INVADERS_LIB__

#include "DiceInvaders.h"

#include <windows.h>
#include <cassert>
#include <cstdio>

class DiceInvadersLib
{
public:
    explicit DiceInvadersLib(const wchar_t* libraryPath)
    {
        m_lib = LoadLibraryW(libraryPath);
        assert(m_lib);

        DiceInvadersFactoryType* factory = (DiceInvadersFactoryType*)GetProcAddress(
            m_lib, "DiceInvadersFactory");
        m_interface = factory();
        assert(m_interface);
    }

    ~DiceInvadersLib()
    {
        FreeLibrary(m_lib);
    }

    IDiceInvaders* get() const
    {
        return m_interface;
    }

private:
    DiceInvadersLib(const DiceInvadersLib&);
    DiceInvadersLib& operator=(const DiceInvadersLib&);

private:
    IDiceInvaders* m_interface;
    HMODULE m_lib;
};
#endif //defined(__DICE_INVADERS_LIB__)

#include "CollisionComponent.hpp"

#include "IGameObject.hpp"
#include "ServiceLocator.hpp"
#include "CollisionManager.hpp"

namespace ea
{
    CollisionComponent::CollisionComponent()
    {
    }


    CollisionComponent::~CollisionComponent()
    {
    }

    void CollisionComponent::init(IGameObject* owner)
    {
        _gameObject = owner;
        init();
    }

    void CollisionComponent::init()
    {
        scheduleFixedUpdate();
    }

    void CollisionComponent::start()
    {
        _collider.circle.setCenter(_gameObject->getPosition());
    }

    void CollisionComponent::update()
    {
        _collider.circle.setCenter(_gameObject->getPosition());
    }

    void CollisionComponent::setEnabled(bool enabled)
    {
        _isEnabled = enabled;
    }

    IGameObject& CollisionComponent::getGameObject() const
    {
        return *_gameObject;
    }

    void CollisionComponent::setOnCollisionDetectedCallback(const OnCollisionDetected& callback)
    {
        _onCollisionDetected = callback;
    }

    bool CollisionComponent::enabled() const
    {
        return _isEnabled;
    }

    void CollisionComponent::setLayerTag(const CollisionLayerTag& layer)
    {
        _layer = layer;
    }

    const CollisionLayerTag& CollisionComponent::getLayerTag() const
    {
        return _layer;
    }

    void CollisionComponent::scheduleUpdate()
    {

    }

    void CollisionComponent::scheduleFixedUpdate()
    {
        auto& collisions = ServiceLocator::get().getCollisionManager();
        collisions.addCollider(this, _layer);
    }

}

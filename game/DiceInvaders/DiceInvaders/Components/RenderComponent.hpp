#ifndef __DiceInvaders__RenderComponent__hpp__
#define __DiceInvaders__RenderComponent__hpp__

#include "RenderComponent.fwd.hpp"

#include "IComponent.hpp"

#include "RenderManager.fwd.hpp"
#include "GameObjectFactory.fwd.hpp"
#include "Types.hpp"

namespace ea
{
    class RenderComponent : public IComponent
    {
        friend class GameObjectFactory;
        friend class IGameObject;
        friend class RenderManager;

    public:
        explicit RenderComponent();
        ~RenderComponent();

        bool enabled() const;
        void setEnabled(bool enabled);
        void init() override;
        void init(IGameObject* owner);
        void start() override;
        void update() override;
        void scheduleDraw();

        // getters
        const Vector2D& getPosition() const;
        const Size& getSize() const;
        const Size& getOffset() const;

    protected:
        IGameObject* _gameObject = nullptr;     // gameobject that owns this component
        ISprite* _spriteHandle = nullptr;
        Vector2D _position;                        // position of the top left corner of the image
        Size _size;                             // size in pixels of the sprite it holds
        Size _offset;                           // precalculated half of _size to center sprite on object position
    };
}

#endif //defined(__DiceInvaders__RenderComponent__hpp__)

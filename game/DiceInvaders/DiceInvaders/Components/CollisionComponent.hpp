#ifndef __DiceInvaders__CollisionComponent__hpp__
#define __DiceInvaders__CollisionComponent__hpp__

#include "CollisionComponent.fwd.hpp"
#include "IComponent.hpp"

#include "CollisionManager.fwd.hpp"
#include "GameObjectFactory.fwd.hpp"
#include "Types.hpp"

namespace ea
{
    class CollisionComponent : public IComponent
    {
        friend class CollisionManager;
        friend class GameObjectFactory;
        friend class IGameObject;

    public:
        explicit CollisionComponent();
        ~CollisionComponent();

        bool enabled() const;
        void setEnabled(bool enabled);
        void init() override;
        void init(IGameObject* owner);
        void start() override;
        void update() override;
        void scheduleFixedUpdate();
        void scheduleUpdate();

        IGameObject& getGameObject() const;

        void setOnCollisionDetectedCallback(const OnCollisionDetected&);

        void setLayerTag(const CollisionLayerTag& layer);
        const CollisionLayerTag& getLayerTag() const;

    private:
        IGameObject* _gameObject = nullptr;         // gameobject that owns this component                         
        CircleCollider _collider;                         // logic shape
        OnCollisionDetected _onCollisionDetected;   // triggers when a collision is detected for this component
        CollisionLayerTag _layer;
    };
}

#endif //defined(__DiceInvaders__CollisionComponent__hpp__)

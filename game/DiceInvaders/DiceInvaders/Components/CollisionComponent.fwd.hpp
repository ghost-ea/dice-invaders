#ifndef __DiceInvaders__CollisionComponent__fwd__
#define __DiceInvaders__CollisionComponent__fwd__

#include "Types.hpp"

namespace ea
{
    class CollisionComponent;

    typedef SafeCallback<const CollisionComponent&> OnCollisionDetected;

    enum class ColliderType
    {
        CircleCollider = 0,
    };

    class CircleCollider
    {
        friend class CollisionComponent;
        friend class CollisionManager;

    public:
        CircleCollider()
            : type(ColliderType::CircleCollider)
            , circle({ {0.0f, 0.0f},{1.0f} }) {}

        CircleCollider(Circle circle)
            : type(ColliderType::CircleCollider)
            , circle(circle) {}

        CircleCollider(Vector2D center, float radius)
            : type(ColliderType::CircleCollider)
            , circle({ center, radius }) {}

        CircleCollider(CircleCollider const &collider)
            : type(collider.type)
            , circle(collider.circle) {}

        CircleCollider(CircleCollider&& collider)
            : type(collider.type)
            , circle(collider.circle) {}

        const ColliderType& getType() const
        {
            return type;
        }
        const Circle& getCircle() const { return circle; };
        void setCenter(Vector2D center) { circle.setCenter(center); };
        void setRadius(float radius) { circle.setRadius(radius); }

    protected:
        ColliderType type;
        Circle circle;
    };
}

#endif //defined(__DiceInvaders__CollisionComponent__fwd__)

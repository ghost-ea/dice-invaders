#include "RenderComponent.hpp"

#include "IGameObject.hpp"
#include "ServiceLocator.hpp"
#include "RenderManager.hpp"

namespace ea
{
    RenderComponent::RenderComponent()
    {
    }

    RenderComponent::~RenderComponent()
    {
    }

    void RenderComponent::init()
    {
        _offset = _size / 2;
        scheduleDraw();
    }

    void RenderComponent::init(IGameObject* owner)
    {
        _gameObject = owner;
        init();
    }

    void RenderComponent::start()
    {

    }

    void RenderComponent::setEnabled(bool enabled)
    {
        _isEnabled = enabled;
    }

    bool RenderComponent::enabled() const
    {
        return _isEnabled;
    }

    void RenderComponent::update()
    {
        _position.x = _gameObject->getPosition().x - _offset.width;
        _position.y = _gameObject->getPosition().y - _offset.height;
    }

    void RenderComponent::scheduleDraw()
    {
        auto& renderer = ServiceLocator::get().getRenderManager();

        renderer.addRenderer(this);
    }

    const Vector2D& RenderComponent::getPosition() const
    {
        return _position;
    }

    const Size& RenderComponent::getSize() const
    {
        return _size;
    }

    const Size& RenderComponent::getOffset() const
    {
        return _offset;
    }
}



#include "LevelManager.hpp"

#include "ServiceLocator.hpp"
#include "ResourceManager.hpp"
#include "RenderManager.hpp"
#include "GameWorld.hpp"
#include "PlayerModel.hpp"

namespace ea
{
    LevelManager::LevelManager()
    {
    }

    LevelManager::~LevelManager()
    {
    }

    bool LevelManager::startUp()
    {
        return true;
    }

    void LevelManager::shutDown()
    {

    }

    void LevelManager::start()
    {
        playerWon = false;
        update();
    }

    void LevelManager::update()
    {
        auto& renderer = ServiceLocator::get().getRenderManager();

        std::string scoreText = "< Score: " + std::to_string(_score.score) + " >";
        GUIComponent scoreGUI = { Vector2D(60.0f, 60.0f), scoreText };
        renderer.addGUI(scoreGUI);

        std::string livesText = "< Lives: " + std::to_string(_player.lives) + " >";
        GUIComponent livesGUI = { Vector2D(60.0f, 120.0f), livesText };
        renderer.addGUI(livesGUI);
    }

    void LevelManager::updateModel(PlayerModel model)
    {
        _player = model;
    }

    void LevelManager::updateScore(ScoreModel score)
    {
        _score = score;
    }

    const PlayerModel& LevelManager::getPlayerModel() const
    {
        return _player;
    }

    const ScoreModel& LevelManager::getScore() const
    {
        return _score;
    }

    const GameWorld& LevelManager::loadLevel(const Data& data)
    {
        _loadedLevel = std::move(data.levelToLoad);
        loadLevelSprites(data.resourcesToLoad);
        _enemyFormationSize = std::move(data._enemyFormationSize);
        return std::move(data.world);
    }

    const Size& LevelManager::getEnemyFormation() const
    {
        return _enemyFormationSize;
    }

    bool LevelManager::loadLevelSprites(const VLevelResources& spritesToLoad)
    {
        auto& resources = ServiceLocator::get().getResourceManager();

        for (const auto& sprite : spritesToLoad)
        {
            resources.loadSprite(sprite);
        }

        return true;
    }
}

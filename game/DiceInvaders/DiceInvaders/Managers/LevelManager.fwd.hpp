#ifndef __DiceInvaders__LevelManager__fwd__
#define __DiceInvaders__LevelManager__fwd__

#include "Levels.hpp"
#include "Types.hpp"

namespace ea
{
    class LevelManager;
}

#endif //defined(__DiceInvaders__LevelManager__fwd__)

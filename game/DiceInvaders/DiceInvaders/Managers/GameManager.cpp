#include "GameManager.hpp"

#include "ServiceLocator.hpp"
#include "CollisionManager.hpp"
#include "RenderManager.hpp"
#include "EngineManager.hpp"
#include "LevelManager.hpp"
#include "RenderManager.hpp"
#include "GameObjectFactory.hpp"
#include "GameWorld.hpp"
#include "EnemyShip.hpp"
#include "RenderComponent.hpp"
#include "ResourceManager.hpp"

#include "Logger.hpp"

namespace ea
{
    std::size_t GameManager::s_nextGameObjectId = 1;

    GameManager::GameManager()
        : _isRunning(true)
        , _isPaused(false)
    {
    }

    GameManager::~GameManager()
    {
    }

    std::size_t GameManager::assignNewId(IGameObject& gameObject)
    {
        gameObject._id = ++GameManager::s_nextGameObjectId;
        return s_nextGameObjectId;
    }

    bool GameManager::startUp()
    {
        _goFactory = GameFactory(new GameObjectFactory());
        return true;
    }

    void GameManager::shutDown()
    {

    }

    bool GameManager::isRunning()
    {
        auto& engine = ServiceLocator::get().getEngineManager();
        return _isRunning && engine.update();
    }

    void GameManager::init()
    {
        GameRandom::initRandom();

        auto& services = ServiceLocator::get();
        auto& levelManager = services.getLevelManager();
        _gameWorld = levelManager.loadLevel(kLevel1); // load all resources needed for the level

        auto& renderer = services.getRenderManager();
        renderer.init();
        auto screenResolution = renderer.getScreenResolution();

        auto& collisions = services.getCollisionManager();
        collisions.init();

        // load scene game objects
        _weaponry[WeaponType::RocketLauncher] = _goFactory->createWeapon(WeaponType::RocketLauncher);
        _weaponry[WeaponType::BombDropper] = _goFactory->createWeapon(WeaponType::BombDropper);

        _player = std::move(_goFactory->createWarShip(GameObjectType::Player));
        _player->loadWeapon(_weaponry[WeaponType::RocketLauncher].get());
        _player->setPosition({ _gameWorld.size.width * 0.5f, _gameWorld.size.height - (float)_player->_renderer->getSize().width });

        _warDirector = std::move(_goFactory->createWarDirector(levelManager.getEnemyFormation()));
        _warDirector->loadEnemyWeapon(_weaponry[WeaponType::BombDropper].get());
        _warDirector->init();
    }

    void GameManager::start()
    {
        auto& services = ServiceLocator::get();
        auto& engine = services.getEngineManager();
        auto& collisions = services.getCollisionManager();

        _lastTime = engine.getTime();
        _deltaTime = 0.0f;
        _fixedDeltatime = 0.0f;

        _player->start();

        for (auto go : _gameObjects)
        {
            go->start();
        }

        collisions.start();
    }

    void GameManager::restart()
    {
        auto& services = ServiceLocator::get();
        auto& levelManager = services.getLevelManager();

        levelManager.start();
        _player->start();
        _warDirector->init();
        _warDirector->start();
        _isPaused = false;
    }

    float GameManager::deltaTime() const
    {
        return _deltaTime;
    }

    void GameManager::update()
    {
        auto& services = ServiceLocator::get();
        auto& engine = services.getEngineManager();
        auto& renderer = services.getRenderManager();
        auto& resources = services.getResourceManager();
        auto& collisions = services.getCollisionManager();
        auto& level = services.getLevelManager();

        float currentTime = engine.getTime();
        _deltaTime = currentTime - _lastTime;  // calculate time step

        _fixedDeltatime += _deltaTime;  // fixed time step

        _deltaTime *= _isPaused ? 0.0f : 1.0f; // mult by zero if paused
        _fixedDeltatime *= _isPaused ? 0.0f : 1.0f;

        if (level.getPlayerModel().lives == 0)
        {
            // game over
            _isPaused = true;

            auto screenResolution = renderer.getScreenResolution();
            std::string gameOverText = "< GAME OVER >";
            GUIComponent livesGUI = { Vector2D(screenResolution.width / 2 - 50, screenResolution.height / 2), gameOverText };
            renderer.addGUI(livesGUI);

            std::string restartText = "< PRESS FIRE TO RESTART >";
            GUIComponent restartTextGUI = { Vector2D(screenResolution.width / 2 - 70 , screenResolution.height / 2 + 40), restartText };
            renderer.addGUI(restartTextGUI);

            if (engine.getKeyPressed(KeyCode::Fire))
            {
                restart();
            }

        }
        else if (level.playerWon)
        {
            restart();
        }

        _player->update(_deltaTime); // player is updated manually for total control

        for (auto go : _gameObjects)
        {
            go->update(_deltaTime);
        }

        if (_fixedDeltatime >= kFixedTimeStep) // update collision/physics with fixed step
        {
            collisions.update(kFixedTimeStep);
            _fixedDeltatime -= kFixedTimeStep;
        }

        level.update();

        renderer.update(_deltaTime);
        // renderer.frustumCulling(); //optional for this game

        resources.getSprite("data/world-ui-bottom-left.bmp")->draw(_gameWorld.origin.x, _gameWorld.origin.y + _gameWorld.size.height - 54);
        resources.getSprite("data/world-ui-bottom-right.bmp")->draw(_gameWorld.origin.x + _gameWorld.size.width - 32, _gameWorld.origin.y + _gameWorld.size.height - 54);

        renderer.draw(); // render all the objects
        renderer.drawGUI(); // render GUI on top

        _lastTime = currentTime;
    }

    void GameManager::pause()
    {
        _isPaused = true;
    }

    void GameManager::resume()
    {
        _isPaused = false;
    }

    const GameObjectFactory& GameManager::getFactory()
    {
        return *_goFactory;
    }

    void GameManager::registerGameObject(IGameObject* gameObject)
    {
#pragma message("check for duplicates to avoid double updates")
        _gameObjects.emplace_back(gameObject);
    }

    const GameWorld& GameManager::getWorld()
    {
        return _gameWorld;
    }
}

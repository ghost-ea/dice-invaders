#ifndef __DiceInvaders__RenderManager__fwd__
#define __DiceInvaders__RenderManager__fwd__

#include "Types.hpp"

namespace ea
{
    class RenderManager;

    struct GUIComponent
    {
        GUIComponent(Vector2D position, Text text)
            : position(position)
            , text(text) {}

        Vector2D position;
        Text text;
    };
}

#endif //defined(__DiceInvaders__RenderManager__fwd__)

#ifndef __DiceInvaders__LevelManager__hpp__
#define __DiceInvaders__LevelManager__hpp__

#include "LevelManager.fwd.hpp"
#include "GameWorld.hpp"
#include "PlayerModel.hpp"
#include "ScoreModel.hpp"

namespace ea
{
    class LevelManager
    {
    public:
        typedef LevelData Data;
        LevelManager();
        ~LevelManager();

        const GameWorld& loadLevel(const Data& data);
        bool startUp();
        void shutDown();
        void start();
        void update();
        const Size& getEnemyFormation() const;
        void updateModel(PlayerModel model);
        const PlayerModel& getPlayerModel() const;
        void updateScore(ScoreModel score);
        const ScoreModel& getScore() const;
        bool playerWon = false;

    private:
        Level _loadedLevel = Level::Level_None;
        Size _enemyFormationSize;
        PlayerModel _player;
        ScoreModel _score;

        bool loadLevelSprites(const VLevelResources& spritesToLoad);
    };
}

#endif //defined(__DiceInvaders__LevelManager__hpp__)


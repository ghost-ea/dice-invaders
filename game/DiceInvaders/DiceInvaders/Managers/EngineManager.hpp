#ifndef __DiceInvaders__EngineManager__hpp
#define __DiceInvaders__EngineManager__hpp

#include "EngineManager.fwd.hpp"
#include "Types.hpp"

namespace ea
{
    enum class KeyCode
    {
        Fire = 0,
        Left,
        Right,
    };

    class EngineManager
    {

    public:
        typedef IDiceInvaders::KeyStatus InputStatus;

        EngineManager();
        ~EngineManager();

        bool startUp(IDiceInvaders* engine);
        void shutDown();
        bool init(int width, int height);
        bool update();
        IDiceInvaders* getEngine() const;
        ISprite* createSprite(const std::string& spriteHandle);
        void drawText(Vector2D position, const std::string& text);
        float getTime(); // total time since engine started
        bool getKeyPressed(KeyCode key);

    private:
        IDiceInvaders* _engine;
        InputStatus _keys;
    };
}
#endif//defined(__DiceInvaders__EngineManager__hpp)


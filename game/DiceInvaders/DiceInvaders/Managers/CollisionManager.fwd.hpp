#ifndef __DiceInvaders__CollisionManager__fwd__
#define __DiceInvaders__CollisionManager__fwd__

#include "Types.hpp"

namespace ea
{
    class CollisionManager;

    enum class CollisionLayerTag
    {
        All = 0,
        Player,
        Enemy,
        Rocket,
        Bomb,
        World,
    };




}

#endif //defined(__DiceInvaders__CollisionManager__fwd__)
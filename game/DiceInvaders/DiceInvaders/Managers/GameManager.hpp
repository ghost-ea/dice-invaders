#ifndef __DiceInvaders__GameManager__hpp__
#define __DiceInvaders__GameManager__hpp__

#include "GameManager.fwd.hpp"

#include "IGameObject.hpp"
#include "IAmmo.hpp"
#include "IWarShip.hpp"
#include "IWeapon.hpp"
#include "IWarDirector.hpp"
#include "GameObjectFactory.fwd.hpp"
#include "EngineManager.fwd.hpp"
#include "GameWorld.hpp"
#include "WarDirector.hpp"
#include "PlayerModel.hpp"

namespace ea
{
    class GameManager
    {
        const float kFixedTimeStep = 0.01f; // fixed time step for collisions

        typedef std::map<WeaponType, std::unique_ptr<IWeapon>> MWeapon;
        typedef std::unique_ptr<IWarShip> WarShip;
        typedef std::vector<WarShip> VWarShip;
        typedef std::vector<IGameObject*> VGameObjects;
        typedef std::unique_ptr<IWarDirector> WDirector;
        typedef std::unique_ptr<GameObjectFactory> GameFactory;

    public:
        GameManager();
        ~GameManager();

        static std::size_t assignNewId(IGameObject& gameObject);

        bool startUp();
        void shutDown();
        bool isRunning();
        void init();
        void start();
        void update();
        void pause();
        void resume();
        void restart();

        float deltaTime() const; // time step to update sistems

        const GameObjectFactory& getFactory();
        void registerGameObject(IGameObject* gameObject);
        const GameWorld& getWorld();

    private:
        static std::size_t s_nextGameObjectId;

        bool _isRunning = true;
        bool _isPaused = false;
        float _lastTime;
        float _deltaTime;
        float _fixedDeltatime;

        GameWorld _gameWorld;
        GameFactory _goFactory;
        WarShip _player;
        MWeapon _weaponry;
        VWarShip _enemies;
        VGameObjects _gameObjects;
        WDirector _warDirector;

    };
}
#endif //defined(__DiceInvaders__GameManager__hpp__)


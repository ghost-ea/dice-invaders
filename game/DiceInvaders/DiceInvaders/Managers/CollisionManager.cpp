#include "CollisionManager.hpp"

#include "CollisionComponent.hpp"

namespace ea
{
    CollisionManager::CollisionManager()
    {
    }


    CollisionManager::~CollisionManager()
    {
    }

    bool CollisionManager::startUp()
    {
        return true;
    }

    void CollisionManager::shutDown()
    {
    }

    void CollisionManager::init()
    {
        _collisionWorld[CollisionLayerTag::Rocket].reserve(10);
        _collisionWorld[CollisionLayerTag::Player].reserve(2);
        _collisionWorld[CollisionLayerTag::Enemy].reserve(60);
        _collisionWorld[CollisionLayerTag::Bomb].reserve(10);
    }

    void CollisionManager::start()
    {
        for (auto collider : _collisionWorld[CollisionLayerTag::Rocket])
        {
            collider->update();
        }

        for (auto collider : _collisionWorld[CollisionLayerTag::Enemy])
        {
            collider->update();
        }

        for (auto collider : _collisionWorld[CollisionLayerTag::Bomb])
        {
            collider->update();
        }

        for (auto collider : _collisionWorld[CollisionLayerTag::Player])
        {
            collider->update();
        }
    }

    void CollisionManager::update(float fixedDeltaTime)
    {
        for (auto& collider : _collisionWorld[CollisionLayerTag::Rocket])
        {
            collider->update();
        }

        for (auto& collider : _collisionWorld[CollisionLayerTag::Enemy])
        {
            collider->update();
        }

        for (auto collider : _collisionWorld[CollisionLayerTag::Bomb])
        {
            collider->update();
        }

        for (auto collider : _collisionWorld[CollisionLayerTag::Player])
        {
            collider->update();
        }

        solveCollisions(fixedDeltaTime);
    }

    void CollisionManager::addCollider(CollisionComponent* collider, CollisionLayerTag layer)
    {
        _collisionWorld[layer].push_back(collider);
    }

    void CollisionManager::solveCollisions(float fixedDeltaTime)
    {
        solveCollisionForLayers(fixedDeltaTime, CollisionLayerTag::Rocket, CollisionLayerTag::Enemy);
        solveCollisionForLayers(fixedDeltaTime, CollisionLayerTag::Bomb, CollisionLayerTag::Player);
        solveCollisionForLayers(fixedDeltaTime, CollisionLayerTag::Enemy, CollisionLayerTag::Player);
    }

    bool CollisionManager::doCirclesTouch(Circle c1, Circle c2)
    {
        double sqDistance = Vector2D::distanceSquared(c1.getCenter(), c2.getCenter());
        double sqRadiusSum = (c1.getRadius() + c2.getRadius()) * (c1.getRadius() + c2.getRadius());

        return sqDistance <= sqRadiusSum;
    }

    void CollisionManager::solveCollisionForLayers(float fixedDeltaTime, CollisionLayerTag from, CollisionLayerTag against)
    {
        if (from == against)
        {
            return; // we don't implement self layer check right now
        }

        for (auto entityFrom : _collisionWorld[from])
        {
            if (!entityFrom->enabled())
            {
                continue;
            }

            const Circle& entityFromCol = entityFrom->_collider.getCircle();

            for (auto entityAgainst : _collisionWorld[against])
            {
                if (!entityAgainst->enabled())
                {
                    continue;
                }

                const Circle& entityAgainstCol = entityAgainst->_collider.getCircle();

                if (doCirclesTouch(entityFromCol, entityAgainstCol))
                {
                    entityFrom->_onCollisionDetected(*entityAgainst);
                    entityAgainst->_onCollisionDetected(*entityFrom);
                }
            }
        }
    }
}

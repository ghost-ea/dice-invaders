#ifndef __DiceInvaders__RenderManager__hpp__
#define __DiceInvaders__RenderManager__hpp__

#include "RenderManager.fwd.hpp"

#include "RenderComponent.fwd.hpp"
#include "Types.hpp"
#include "GameWorld.hpp"

namespace ea
{
    class RenderManager
    {
    private:
        Size _screenResolution;
        GameWorld _worldLimits; // for object culling
        std::vector <RenderComponent*> _renderers;
        std::queue<GUIComponent> _guiElements;

    public:
        RenderManager();
        ~RenderManager();

        bool startUp();
        void shutDown();

        void init();
        void start();
        void update(float deltaTime);
        void addRenderer(RenderComponent* renderer);
        void addGUI(GUIComponent& gui);
        void frustumCulling();
        void draw();
        void drawGUI();

        const Size& getScreenResolution() const;
    };
}
#endif //defined(__DiceInvaders__RenderManager__hpp__)



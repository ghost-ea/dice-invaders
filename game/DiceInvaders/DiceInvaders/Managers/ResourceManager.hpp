#ifndef __DiceInvaders__ResourceManager__hpp__
#define __DiceInvaders__ResourceManager__hpp__

#include "ResourceManager.fwd.hpp"

#include "Types.hpp"
#include "UserConfig.hpp"

namespace ea
{
    class ResourceManager
    {
    public:
        typedef std::map<std::string, ISprite*> MSprites;
        ResourceManager();
        ~ResourceManager();

        bool startUp();
        void shutDown();
        // void loadResources();
        const UserConfig& getUserConfig() const;
        bool loadSprite(const std::string& url);
        ISprite* getSprite(const std::string& spriteHandle);
        Size getSpriteSize(const std::string& spriteHandle);

    private:
        MSprites _sprites;
    };
}
#endif //defined(__DiceInvaders__ResourceManager__hpp__)



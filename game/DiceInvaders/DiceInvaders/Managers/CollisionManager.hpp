#ifndef __DiceInvaders__CollisionManager__hpp__
#define __DiceInvaders__CollisionManager__hpp__

#include "CollisionManager.fwd.hpp"

#include "CollisionComponent.fwd.hpp"

namespace ea
{
    class CollisionManager
    {
        typedef std::vector<CollisionComponent*> VColliders;
        typedef std::map<CollisionLayerTag, VColliders> MVColliders;


    public:
        CollisionManager();
        ~CollisionManager();

        bool startUp();
        void shutDown();

        void init();
        void start();
        void update(float fixedDeltaTime);

        void addCollider(CollisionComponent* collider, CollisionLayerTag layer);

    private:
        MVColliders _collisionWorld;

        void solveCollisions(float fixedDeltaTime);
        //void solveCollisionForLayer();
        void solveCollisionForLayers(float fixedDeltaTime, CollisionLayerTag from, CollisionLayerTag against);

        bool doCirclesTouch(Circle c1, Circle c2);
    };
}

#endif //defined(__DiceInvaders__CollisionManager__hpp__)


#include "ResourceManager.hpp"

#include "ServiceLocator.hpp"
#include "EngineManager.hpp"
#include "Types.hpp"
#include "UserConfig.hpp"

namespace ea
{
    namespace
    {
        const char* kMissingResource = "data/missing-sprite.bmp";
    }

    ResourceManager::ResourceManager()
    {
    }

    ResourceManager::~ResourceManager()
    {
    }

    bool ResourceManager::startUp()
    {
        return true;
    }

    void ResourceManager::shutDown()
    {
        for (auto sprite = _sprites.begin(); sprite != _sprites.end(); ++sprite)
        {
            sprite->second->destroy();
        }
    }

    const UserConfig& ResourceManager::getUserConfig() const
    {
        return kUserConfig;
    }

    bool ResourceManager::loadSprite(const std::string& url)
    {
        auto& engine = ServiceLocator::get().getEngineManager();
        _sprites[url] = engine.createSprite(url);

        return true;
    }

    ISprite* ResourceManager::getSprite(const std::string& spriteHandle)
    {
        if (_sprites.find(spriteHandle) != _sprites.end())
        {
            return _sprites.at(spriteHandle);
        }

        return _sprites.at(kMissingResource);
    }

    Size ResourceManager::getSpriteSize(const std::string& spriteHandle)
    {
        // this is actually fake right now because we only support 32x32 sprites
        return Size(32, 32);
    }




}

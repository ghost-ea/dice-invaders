#include "EngineManager.hpp"

namespace ea
{
    EngineManager::EngineManager()
    {
    }

    EngineManager::~EngineManager()
    {
    }

    bool EngineManager::startUp(IDiceInvaders* engine)
    {
        _engine = engine;
        return true;
    }

    void EngineManager::shutDown()
    {

    }

    float EngineManager::getTime()
    {
        return _engine->getElapsedTime();
    }

    bool EngineManager::init(int width, int height)
    {
        return _engine->init(width, height);
    }

    bool EngineManager::update()
    {
        _engine->getKeyStatus(_keys);
        return _engine->update();
    }

    ISprite* EngineManager::createSprite(const std::string& spriteHandle)
    {
        if (_engine != nullptr)
        {
            return _engine->createSprite(spriteHandle.c_str());
        }

#pragma message("should print a message of error creating sprite")
        return nullptr;
    }

    void EngineManager::drawText(Vector2D position, const std::string& text)
    {
        _engine->drawText((int)position.x, (int)position.y, text.c_str());
    }

    bool EngineManager::getKeyPressed(KeyCode key)
    {
        switch (key)
        {
            case KeyCode::Fire:
            {
                return _keys.fire;
            }
            case KeyCode::Left:
            {
                return _keys.left;
            }
            case KeyCode::Right:
            {
                return _keys.right;
            }
        }
    }
}



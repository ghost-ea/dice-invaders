#include "RenderManager.hpp"

#include "RenderComponent.hpp"
#include "ServiceLocator.hpp"
#include "EngineManager.hpp"
#include "GameManager.hpp"
#include "ResourceManager.hpp"
#include "GameWorld.hpp"
#include "Types.hpp"

namespace ea
{
    RenderManager::RenderManager()
    {
    }

    RenderManager::~RenderManager()
    {
    }

    bool RenderManager::startUp()
    {
        auto& engine = ServiceLocator::get().getEngineManager();
        auto& resources = ServiceLocator::get().getResourceManager();
        _screenResolution = resources.getUserConfig().screenResolution;

        return engine.init(_screenResolution.width, _screenResolution.height);
    }

    void RenderManager::shutDown()
    {
    }

    void RenderManager::init()
    {
        auto& game = ServiceLocator::get().getGameManager();
        _worldLimits = game.getWorld();
    }

    void RenderManager::start()
    {

    }

#pragma message("Remember to check delta time for animations")
    void RenderManager::update(float deltaTime)
    {
        for (auto renderer : _renderers)
        {
            renderer->update();
        }
    }

    void RenderManager::addRenderer(RenderComponent* renderer)
    {
        _renderers.push_back(renderer);
    }

    void RenderManager::frustumCulling()
    {
        for (auto renderer : _renderers)
        {
            renderer->setEnabled(GameWorldUtils::contains(_worldLimits, renderer->_position, renderer->_size));
        }
    }

    void RenderManager::draw()
    {
        for (auto renderer : _renderers)
        {
            if (!renderer->enabled())
            {
                continue;
            }

            renderer->_spriteHandle->draw((int)(_worldLimits.origin.x + renderer->_position.x)
                , (int)(_worldLimits.origin.y + renderer->_position.y));
        }
    }

    void RenderManager::addGUI(GUIComponent& gui)
    {
        _guiElements.push(gui);
    }

    void RenderManager::drawGUI()
    {
        auto& engine = ServiceLocator::get().getEngineManager();
        while (!_guiElements.empty())
        {
            auto& gui = _guiElements.front();
            engine.drawText(gui.position, gui.text);
            _guiElements.pop();
        }
    }

    const Size& RenderManager::getScreenResolution() const
    {
        return _screenResolution;
    }
}

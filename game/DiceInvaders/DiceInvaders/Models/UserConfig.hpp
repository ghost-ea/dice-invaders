#ifndef __DiceInvaders__UserConfig__hpp__
#define __DiceInvaders__UserConfig__hpp__

#include "Types.hpp"

namespace ea
{
    struct UserConfig
    {
        Size screenResolution;
    };

    //const UserConfig kUserConfig = { Size(600, 690) };
    const UserConfig kUserConfig = { Size(1024, 768) };
}

#endif //defined(__DiceInvaders__UserConfig__hpp__)

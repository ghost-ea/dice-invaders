#ifndef __DiceInvaders__ScoreModel__hpp__
#define __DiceInvaders__ScoreModel__hpp__

#include "Types.hpp"

namespace ea
{

    struct ScoreModel
    {
        ScoreModel() : score(0) {}
        ScoreModel(int score) : score(score) {}

        int score = 0;
    };
}

#endif //defined(__DiceInvaders__ScoreModel__hpp__)
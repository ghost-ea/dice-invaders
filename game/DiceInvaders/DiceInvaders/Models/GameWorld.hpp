#ifndef __DiceInvaders__GameWorld__hpp__
#define __DiceInvaders__GameWorld__hpp__

#include "Types.hpp"

namespace ea
{
    struct GameWorld
    {
        GameWorld() : origin(0.0f, 0.0f), size(600, 690) {}

        GameWorld(Vector2D origin, Size size)
            : origin(origin)
            , size(size) {}

        Vector2D origin = { 0.0f, 0.0f };
        Size size = { 600, 690 };
    };

    class GameWorldUtils
    {
    public:
        static bool contains(GameWorld world, const Vector2D& position, const Size& size)
        {
            return position.x < world.size.width &&
                position.x + size.width > 0 &&
                position.y < world.size.height &&
                position.y + size.height > 0;
        }
    private:
        GameWorldUtils();
    };
}

#endif //defined(__DiceInvaders__GameWorld__hpp__)
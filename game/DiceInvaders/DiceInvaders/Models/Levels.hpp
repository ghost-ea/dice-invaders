#ifndef __DiceInvaders__Leves__hpp__
#define __DiceInvaders__Leves__hpp__

// Models that describe a level with the assets needed
#include "Types.hpp"
#include "GameWorld.hpp"

namespace ea
{
    enum class Level
    {
        Level_None = 0,
        Level_1,
        Level_2,
    };

    typedef std::vector<std::string> VLevelResources;
    struct LevelData
    {
        Level levelToLoad;
        VLevelResources resourcesToLoad;
        GameWorld world;
        Size _enemyFormationSize;
    };

    // This is our levels database
    const LevelData kLevel1 = { Level::Level_1,
    { "data/missing-sprite.bmp", // reosources
        "data/player.bmp",
        "data/bomb.bmp",
        "data/enemy1.bmp",
        "data/enemy2.bmp",
        "data/rocket.bmp",
        "data/world-ui-top-left.bmp",
        "data/world-ui-bottom-left.bmp",
        "data/world-ui-bottom-right.bmp",
    },
    {
            {212.0f,0.0f},{600,690}         // game world origin and size
    },

            {11, 5}                        // enemy formation size width x height
    ,
    };
}

#endif //defined
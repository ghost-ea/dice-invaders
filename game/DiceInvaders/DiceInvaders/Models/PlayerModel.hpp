#ifndef __DiceInvaders__PlayerModel__hpp__
#define __DiceInvaders__PlayerModel__hpp__

#include "Types.hpp"

namespace ea
{
    namespace
    {
        const int kDefaultLives = 3;
    }

    struct PlayerModel
    {
        PlayerModel() : lives(kDefaultLives) {}
        PlayerModel(int lives) : lives(lives) {}

        int lives = kDefaultLives;
    };
}

#endif //defined(__DiceInvaders__PlayerModel__hpp__)
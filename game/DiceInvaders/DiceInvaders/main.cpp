#include <windows.h>
#include <cassert>
#include <cstdio>
#include <memory>

#include "ServiceLocator.hpp"
#include "GameManager.hpp"

using namespace ea;

int APIENTRY WinMain(
    _In_ HINSTANCE instance,
    _In_opt_ HINSTANCE previousInstance,
    _In_ LPSTR commandLine,
    _In_ int commandShow)
{
    DiceInvadersLib lib(L"DiceInvaders.dll");
    static IDiceInvaders* system = lib.get();

    auto& services = ServiceLocator::get();
    if (!services.startUp(system))
    {
        return -1;
    }

    auto& game = services.getGameManager();

    game.init();
    game.start();

    while (game.isRunning())
    {
        game.update();
    }

    services.shutDown();
    system->destroy();

    return 0;
}

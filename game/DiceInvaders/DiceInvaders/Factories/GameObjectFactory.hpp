#ifndef __DiceInvaders__GameObjectFactory__hpp__
#define __DiceInvaders__GameObjectFactory__hpp__

#include "GameObjectFactory.fwd.hpp"

#include "IGameObject.hpp"
#include "IWeapon.hpp"
#include "IWarShip.hpp"
#include "IAmmo.hpp"
#include "IWarDirector.hpp"

#include "RenderComponent.fwd.hpp"
#include "CollisionManager.fwd.hpp"
#include "CollisionComponent.fwd.hpp"

namespace ea
{
    class GameObjectFactory
    {
    public:
        GameObjectFactory();
        ~GameObjectFactory();

        std::unique_ptr<IWarShip> createWarShip(GameObjectType type);
        std::unique_ptr<IWeapon> createWeapon(WeaponType type);
        std::unique_ptr<IWarDirector> createWarDirector(const Size& enemyFormationSize);

    private:
        std::unique_ptr<IWarShip> createPlayer();
        std::unique_ptr<IWarShip> createEmemy(GameObjectType type);
        std::unique_ptr<IAmmo> createRocket();
        std::unique_ptr<IWeapon> createRocketPool();
        std::unique_ptr<IAmmo> createBomb();
        std::unique_ptr<IWeapon> createBombPool();
        IWarDirector::VWarShip createEnemyFormation(const Size& enemyFormationSize);
    };
}
#endif //defined(__DiceInvaders__GameObjectFactory__hpp__)


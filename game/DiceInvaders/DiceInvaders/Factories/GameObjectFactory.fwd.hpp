#ifndef __DiceInvaders__GameObjectFactory__fwd__
#define __DiceInvaders__GameObjectFactory__fwd__

namespace ea
{
    class GameObjectFactory;

    enum class GameObjectType
    {
        Player,
        Enemy1,
        Enemy2,
        Bomb,
        Rocket,
    };

    enum class WeaponType
    {
        RocketLauncher,
        BombDropper,
    };
}

#endif //defined(__DiceInvaders__GameObjectFactory__fwd__)
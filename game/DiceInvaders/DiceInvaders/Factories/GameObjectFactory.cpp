#include "GameObjectFactory.hpp"

#include "GameManager.hpp"
#include "CollisionManager.hpp"
#include "ServiceLocator.hpp"
#include "ResourceManager.hpp"
#include "RenderComponent.hpp"
#include "CollisionComponent.hpp"
#include "Types.hpp"
#include "PlayerShip.hpp"
#include "EnemyShip.hpp"
#include "Rocket.hpp"
#include "RocketPool.hpp"
#include "Bomb.hpp"
#include "BombPool.hpp"
#include "WarDirector.hpp"

namespace ea
{
    GameObjectFactory::GameObjectFactory()
    {
    }


    GameObjectFactory::~GameObjectFactory()
    {
    }

    std::unique_ptr<IWarShip> GameObjectFactory::createWarShip(GameObjectType type)
    {
        auto& resources = ServiceLocator::get().getResourceManager();
        auto& gameManager = ServiceLocator::get().getGameManager();
        switch (type)
        {
            case GameObjectType::Player:
            {
                std::unique_ptr<IWarShip> player = createPlayer();
                return player;
            }

            case GameObjectType::Enemy1:
            case GameObjectType::Enemy2:
            {
                std::unique_ptr<IWarShip> enemy = createEmemy(type);
                return enemy;
            }
        }
    }

    std::unique_ptr<IWeapon> GameObjectFactory::createWeapon(WeaponType type)
    {
        switch (type)
        {
            case WeaponType::RocketLauncher:
            {
                return createRocketPool();
            }
            case WeaponType::BombDropper:
            {
                return createBombPool();
            }
        }
    }

    std::unique_ptr<IWarShip> GameObjectFactory::createPlayer()
    {
        auto& resources = ServiceLocator::get().getResourceManager();

        std::string spriteHandle = "data/player.bmp";
        CollisionLayerTag layer = CollisionLayerTag::Player;
        std::unique_ptr<IWarShip> player(new PlayerShip());

        GameManager::assignNewId(*player.get());
        player->setName("PlayerShip");

        // Game Object Init
        player->init();
        // Renderer Init - assign a sprite and register it with render manager
        player->_renderer->_position = Vector2D::Zero();
        player->_renderer->_spriteHandle = resources.getSprite(spriteHandle);
        player->_renderer->_size = resources.getSpriteSize(spriteHandle);
        player->_renderer->init(player.get());
        // Collider init - assigna collision shape and register with collision manager
        player->_collider->_layer = layer;
        player->_collider->_collider.setRadius(12.0f);
        player->_collider->setOnCollisionDetectedCallback(player->getCollisionCallback());
        player->_collider->init(player.get());
        // Input component init ... assign player 1 or player 2
        // player do not register with game manager because he receive a special treatment
        return player; // caller has to register manually to the game manager
    }

    std::unique_ptr<IWarShip> GameObjectFactory::createEmemy(GameObjectType type)
    {
        auto& resources = ServiceLocator::get().getResourceManager();
        auto& game = ServiceLocator::get().getGameManager();

        // default sprite
        std::string spriteHandle = "data/enemy1.bmp";
        CollisionLayerTag layer = CollisionLayerTag::Enemy;
        std::unique_ptr<IWarShip> enemy(new EnemyShip());

        switch (type)
        {
            case GameObjectType::Enemy1:
            {
                spriteHandle = "data/enemy1.bmp";
                break;
            }
            case GameObjectType::Enemy2:
            {
                spriteHandle = "data/enemy2.bmp";
                break;
            }
        }

        GameManager::assignNewId(*enemy.get());
        enemy->setName("EnemyShip_" + std::to_string(enemy->_id));

        // Game Object Init
        enemy->init();
        // Renderer Init - assign a sprite and register it with render manager
        enemy->_renderer->_position = Vector2D::Zero();
        enemy->_renderer->_spriteHandle = resources.getSprite(spriteHandle);
        enemy->_renderer->_size = resources.getSpriteSize(spriteHandle);
        enemy->_renderer->init(enemy.get());
        // Collider init - assigna collision shape and register with collision manager
        enemy->_collider->_layer = layer;
        enemy->_collider->_collider.setRadius(12.0f);
        enemy->_collider->setOnCollisionDetectedCallback(enemy->getCollisionCallback());
        enemy->_collider->init(enemy.get());

        // Register this object with the game manager to receive start/update
        game.registerGameObject(enemy.get());
        return enemy;
    }

    std::unique_ptr<IAmmo> GameObjectFactory::createRocket()
    {
        auto& resources = ServiceLocator::get().getResourceManager();
        auto& game = ServiceLocator::get().getGameManager();

        // Model
        std::string spriteHandle = "data/rocket.bmp";
        CollisionLayerTag layer = CollisionLayerTag::Rocket;
        std::unique_ptr<IAmmo> rocket(new Rocket());

        GameManager::assignNewId(*rocket.get());
        rocket->setName("Rocket_" + std::to_string(rocket->_id));

        // Game Object Init
        rocket->init();
        // Renderer Init - assign a sprite and register it with render manager
        rocket->_renderer->_position = Vector2D::Zero();
        rocket->_renderer->_spriteHandle = resources.getSprite(spriteHandle);
        rocket->_renderer->_size = resources.getSpriteSize(spriteHandle);
        rocket->_renderer->init(rocket.get());
        // Collider init - assigna collision shape and register with collision manager
        rocket->_collider->_layer = layer;
        rocket->_collider->_collider.setRadius(6.0f);
        rocket->_collider->_collider.setCenter({ 0.0f, 10.0f });
        rocket->_collider->setOnCollisionDetectedCallback(rocket->getCollisionCallback());
        rocket->_collider->init(rocket.get());

        // Register this object with the game manager to receive start/update
        game.registerGameObject(rocket.get());
        return rocket;
    }

    std::unique_ptr<IWeapon> GameObjectFactory::createRocketPool()
    {
        auto& resources = ServiceLocator::get().getResourceManager();
        auto& game = ServiceLocator::get().getGameManager();

        std::unique_ptr<IWeapon> weapon{ new RocketPool{} };

        GameManager::assignNewId(*weapon.get());
        weapon->setName("RocketPool_" + std::to_string(weapon->_id));

        for (std::size_t i = 0; i < 10; ++i)
        {
            weapon->loadAmmo(createRocket());
        }

        game.registerGameObject(weapon.get());

        return weapon;
    }

    std::unique_ptr<IAmmo> GameObjectFactory::createBomb()
    {
        auto& resources = ServiceLocator::get().getResourceManager();
        auto& game = ServiceLocator::get().getGameManager();

        // Model
        std::string spriteHandle = "data/bomb.bmp";
        CollisionLayerTag layer = CollisionLayerTag::Bomb;
        std::unique_ptr<IAmmo> bomb(new Bomb());

        GameManager::assignNewId(*bomb.get());
        bomb->setName("Bomb_" + std::to_string(bomb->_id));

        // Game Object Init
        bomb->init();
        // Renderer Init - assign a sprite and register it with render manager
        bomb->_renderer->_position = Vector2D::Zero();
        bomb->_renderer->_spriteHandle = resources.getSprite(spriteHandle);
        bomb->_renderer->_size = resources.getSpriteSize(spriteHandle);
        bomb->_renderer->init(bomb.get());
        // Collider init - assigna collision shape and register with collision manager
        bomb->_collider->_layer = layer;
        bomb->_collider->_collider.setRadius(5.0f);
        bomb->_collider->_collider.setCenter({ 0.0f, -3.0f });
        bomb->_collider->setOnCollisionDetectedCallback(bomb->getCollisionCallback());
        bomb->_collider->init(bomb.get());

        // Register this object with the game manager to receive start/update
        game.registerGameObject(bomb.get());
        return bomb;
    }

    std::unique_ptr<IWeapon> GameObjectFactory::createBombPool()
    {
        auto& resources = ServiceLocator::get().getResourceManager();
        auto& game = ServiceLocator::get().getGameManager();

        std::unique_ptr<IWeapon> weapon{ new BombPool{} };

        GameManager::assignNewId(*weapon.get());
        weapon->setName("BombPool_" + std::to_string(weapon->_id));

        for (std::size_t i = 0; i < 30; ++i)
        {
            weapon->loadAmmo(createBomb());
        }

        game.registerGameObject(weapon.get());

        return weapon;
    }

    std::unique_ptr<IWarDirector> GameObjectFactory::createWarDirector(const Size& enemyFormationSize)
    {
        auto& game = ServiceLocator::get().getGameManager();

        std::unique_ptr<IWarDirector> director{ new WarDirector{} };
        director->_enemyFormationSize = enemyFormationSize;
        director->loadEnemyFormation(std::move(createEnemyFormation(enemyFormationSize)));

        game.registerGameObject(director.get());
        return director;
    }

    IWarDirector::VWarShip GameObjectFactory::createEnemyFormation(const Size& enemyFormationSize)
    {
        std::size_t totalEnemies = enemyFormationSize.width * enemyFormationSize.height;
        IWarDirector::VWarShip enemyFormation;
        enemyFormation.reserve(totalEnemies);

        for (std::size_t i = 0; i < totalEnemies; ++i)
        {
            GameObjectType type = (((int)i / enemyFormationSize.width) % 2) == 0 ? GameObjectType::Enemy1 : GameObjectType::Enemy2;
            enemyFormation.emplace_back(createEmemy(type));
        }

        return enemyFormation;
    }
}

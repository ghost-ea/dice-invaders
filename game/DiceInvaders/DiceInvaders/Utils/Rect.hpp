#ifndef __DiceInvaders__Types__Rect__hpp__
#define __DiceInvaders__Types__Rect__hpp__

#include <cmath>
#include "Vector2D.hpp"

namespace ea
{
    class Rect
    {
    public:
        Rect(int x = 0, int y = 0, int w = 0, int h = 0);
        ~Rect(void) {};

        inline int Left(void) const { return x; }
        inline int Right(void) const { return x + w; }
        inline int Top(void) const { return y; }
        inline int Bottom(void) const { return y + h; }

        bool Contains(Vector2D& vVec) const;
        bool Contains(int x, int y) const;

        static Rect Empty();

        static Vector2D GetIntersectionDepth(const Rect& rectA, const Rect& rectB);
        static Vector2D GetBottomCenter(const Rect& rect);
        static Vector2D GetCenter(const Rect& rect);
        static float GetDistance(const Rect& rectA, const Rect& rectB);
        static Vector2D GetDirection(const Rect& rectA, const Rect& rectB);

        Rect& operator= (const Rect& r2);

        bool operator== (const Rect& r2) const;
        bool operator!= (const Rect& r2) const;

    public:
        int x, y, w, h;
    };
}

#endif //defined(__DiceInvaders__Types__Rect__hpp__)
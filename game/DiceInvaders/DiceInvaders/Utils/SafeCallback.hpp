#ifndef __DiceInvaders__SafeCallback__hpp__
#define __DiceInvaders__SafeCallback__hpp__

#include <cstddef>
#include <functional>

namespace ea
{
    template <class... Args>
    class SafeCallback
    {
    public:
        SafeCallback()
        {
        }

        template <typename T>
        SafeCallback(T&& f, typename std::enable_if<!std::is_same<typename std::decay<T>::type, SafeCallback>::value>::type* = nullptr)
            : _f(std::forward<T>(f))
        {
        }

        SafeCallback(const SafeCallback& other) = default;
        SafeCallback(SafeCallback&& other) = default;

        SafeCallback& operator=(const SafeCallback& other) = default;
        SafeCallback& operator=(SafeCallback&& other) = default;

        template <class... T>
        void operator()(T&&... args) const
        {
            if (_f)
            {
                _f(std::forward<T>(args)...);
            }
        }

        explicit operator bool() const
        {
            return (bool)_f;
        }

        bool operator==(std::nullptr_t) const
        {
            return _f == nullptr;
        }

        bool operator!=(std::nullptr_t) const
        {
            return _f != nullptr;
        }

        operator std::function<void(Args...)>&()
        {
            return _f;
        }

        operator const std::function<void(Args...)>&() const
        {
            return _f;
        }

    private:
        std::function<void(Args...)> _f;
    };

    typedef SafeCallback<> Callback;
}

#endif //defined(__DiceInvaders__SafeCallback__hpp__)
#ifndef __DiceInvaders__Types__Vector2D__hpp__
#define __DiceInvaders__Types__Vector2D__hpp__

#include <cmath>

namespace ea
{
    struct Vector2D
    {

    public:
        Vector2D() : x(0), y(0) {}
        Vector2D(float x, float y) : x(x), y(y) {}

        inline static Vector2D Vector2D::Zero() { return Vector2D(0, 0); };

        inline static float distance(const Vector2D& v1, const Vector2D& v2)
        {
            return static_cast<float>(sqrtf(distanceSquared(v1, v2)));
        }

        inline static double distanceSquared(const Vector2D& v1, const Vector2D& v2)
        {
            return pow((v2.x - v1.x), 2) + pow((v2.y - v1.y), 2);
        }

    public:

        float distance(const Vector2D& v2)
        {
            return static_cast<float>(sqrtf(distanceSquared(*this, v2)));
        }

        double distanceSquared(const Vector2D& v2)
        {
            return pow((v2.x - x), 2) + pow((v2.y - y), 2);
        }

        void Rotate(const float angle)
        {
            float xt = (x * cosf(angle)) - (y * sinf(angle));
            float yt = (y * cosf(angle)) + (x * sinf(angle));
            x = xt;
            y = yt;
        }

        float Magnitude() const
        {
            return sqrtf(x * x + y * y);
        }

        float Normalize()
        {
            float mag = Magnitude();

            if (mag != 0.0)
            {
                x /= mag;
                y /= mag;
            }

            return mag;
        }

        float DotProduct(const Vector2D& v2) const
        {
            return (x * v2.x) + (y * v2.y);
        }

        float CrossProduct(const Vector2D& v2) const
        {
            return (x * v2.y) - (y * v2.x);
        }

        Vector2D& operator= (const Vector2D& v2)
        {
            if (this == &v2)
                return *this;

            x = v2.x;
            y = v2.y;

            return *this;
        };

        Vector2D& operator+= (const Vector2D& v2)
        {
            x += v2.x;
            y += v2.y;

            return *this;
        };

        Vector2D& operator-= (const Vector2D& v2)
        {
            x -= v2.x;
            y -= v2.y;

            return *this;
        };

        Vector2D& operator*= (const float scalar)
        {
            x *= scalar;
            y *= scalar;

            return *this;
        };

        Vector2D& operator/= (const float scalar)
        {
            x /= scalar;
            y /= scalar;

            return *this;
        };

        const Vector2D operator+(const Vector2D &v2) const
        {
            return Vector2D(*this) += v2;
        };

        const Vector2D operator-(const Vector2D &v2) const
        {
            return Vector2D(*this) -= v2;
        };

        const Vector2D operator*(const float scalar) const
        {
            return Vector2D(*this) *= scalar;
        };

        const Vector2D operator/(const float scalar) const
        {
            return Vector2D(*this) /= scalar;
        };

        bool operator== (const Vector2D& v2) const
        {
            return ((x == v2.x) && (y == v2.y));
        }

        bool operator!= (const Vector2D& v2) const
        {
            return !((x == v2.x) && (y == v2.y));
        };

    public:
        float x = 0.0f, y = 0.0f;
    };
}

#endif// defined(__DiceInvaders__Types__Vector2D__hpp__)
#ifndef __DiceInvaders__Types__hpp__
#define __DiceInvaders__Types__hpp__

#include <cmath>
#include <functional>
#include <map>
#include <memory>
#include <queue>
#include <random>
#include <string>
#include <vector>

#include "Circle.hpp"
#include "DiceInvadersLib.hpp"
#include "Rect.hpp"
#include "SafeCallback.hpp"
#include "Size.hpp"
#include "Vector2D.hpp"

namespace ea
{
    typedef std::string Text;

#define SAFE_DELETE(service) \
    if(service)              \
    {                        \
        delete service;      \
        service = nullptr;   \
    };


    namespace GameRandom
    {
        static std::uniform_int_distribution<int> randomDistributionInt{ 1,6 };
        static std::uniform_real_distribution<float> randomDistribution;
        static std::mt19937 randomGenerator;
        static std::random_device randomDevice;

        static void initRandom()
        {
            randomGenerator = std::mt19937(randomDevice());
        }

        static float getRandomFloat(std::uniform_real_distribution<float> dist)
        {
            return dist(randomGenerator);
        }

        static int getRandomDice()
        {
            return randomDistributionInt(randomGenerator);
        }
    }

}

#endif //defined(__DiceInvaders__Types__hpp__)



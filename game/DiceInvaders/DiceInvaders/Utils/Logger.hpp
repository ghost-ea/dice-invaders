#ifndef __DiceInvaders__Logger__hpp__
#define __DiceInvaders__Logger__hpp__

#include <windows.h>
#include <iostream>
#include <sstream>

#ifdef _DEBUG
#define DBOUT( s )                          \
{                                           \
   std::wostringstream os_;                 \
   os_ << std::endl << s;                   \
   OutputDebugStringW( os_.str().c_str() ); \
}
#else
#define  DBOUT( s ){}
#endif

#endif //defined(__DiceInvaders__Logger__hpp__)
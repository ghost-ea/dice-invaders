#include "Rect.hpp"

namespace ea
{
    Rect::Rect(int x, int y, int w, int h)
    {
        this->x = x;
        this->y = y;
        this->w = w;
        this->h = h;
    }

    //-----------------------------------------------------------------------------
    // Purpose: Check if rectangle contains a 2D vector
    //-----------------------------------------------------------------------------
    bool Rect::Contains(Vector2D& vVec) const
    {
        if ((vVec.x >= x) &&
            (vVec.x <= x + w) &&
            (vVec.y >= y) &&
            (vVec.x <= y + h))
        {
            return true;
        }
        else
            return false;
    }

    //-----------------------------------------------------------------------------
    // Purpose: Check if rectangle contains a set of coords
    //-----------------------------------------------------------------------------
    bool Rect::Contains(int x, int y) const
    {
        if ((x >= this->x) &&
            (x <= this->x + this->w) &&
            (y >= this->y) &&
            (x <= this->y + this->h))
        {
            return true;
        }
        else
            return false;
    }

    //-----------------------------------------------------------------------------
    // Purpose: Return an empty rectangle
    //-----------------------------------------------------------------------------
    Rect Rect::Empty()
    {
        return Rect();
    }

    //-----------------------------------------------------------------------------
    // Purpose: Get intersection depth between two rectangles
    //-----------------------------------------------------------------------------
    Vector2D Rect::GetIntersectionDepth(const Rect& rectA, const Rect& rectB)
    {
        // Calculate half sizes.
        float halfWidthA = rectA.w * 0.5f;
        float halfHeightA = rectA.h * 0.5f;
        float halfWidthB = rectB.w * 0.5f;
        float halfHeightB = rectB.h * 0.5f;

        // Calculate centers.
        Vector2D centerA(rectA.x + halfWidthA, rectA.y + halfHeightA);
        Vector2D centerB(rectB.x + halfWidthB, rectB.y + halfHeightB);

        // Calculate current and minimum-non-intersecting distances between centers.
        float distanceX = centerA.x - centerB.x;
        float distanceY = centerA.y - centerB.y;
        float minDistanceX = halfWidthA + halfWidthB;
        float minDistanceY = halfHeightA + halfHeightB;

        // If we are not intersecting at all, return (0, 0).
        if (abs(distanceX) >= minDistanceX || abs(distanceY) >= minDistanceY)
            return Vector2D::Zero();

        // Calculate and return intersection depths.
        float depthX = distanceX > 0 ? minDistanceX - distanceX : -minDistanceX - distanceX;
        float depthY = distanceY > 0 ? minDistanceY - distanceY : -minDistanceY - distanceY;
        return Vector2D(depthX, depthY);
    }

    //-----------------------------------------------------------------------------
    // Purpose: Gets the position of the center of the bottom edge of the rectangle.
    //-----------------------------------------------------------------------------
    Vector2D Rect::GetBottomCenter(const Rect& rect)
    {
        return Vector2D((float)(rect.x + rect.w * 0.5f), (float)(rect.y + rect.h));
    }

    //-----------------------------------------------------------------------------
    // Purpose: Gets the position of the center point of a rectangle
    //-----------------------------------------------------------------------------
    Vector2D Rect::GetCenter(const Rect& rect)
    {
        return Vector2D((float)(rect.x + rect.w * 0.5f), (float)(rect.y + rect.h * 0.5f));
    }

    //-----------------------------------------------------------------------------
    // Purpose: Gets the floating point distance between the center point
    //          of one rectangle and the center point of another.
    //-----------------------------------------------------------------------------
    float Rect::GetDistance(const Rect& rectA, const Rect& rectB)
    {
        return Vector2D::distance(GetCenter(rectA), GetCenter(rectB));
    }

    //-----------------------------------------------------------------------------
    // Purpose: Gets the unit vector from one rectangle to another
    //-----------------------------------------------------------------------------
    Vector2D Rect::GetDirection(const Rect& rectA, const Rect& rectB)
    {
        Vector2D direction = GetCenter(rectA) - GetCenter(rectB);
        direction.Normalize();
        return direction;
    }

    Rect& Rect::operator= (const Rect& r2)
    {
        if (this == &r2)
            return *this;

        x = r2.x;
        y = r2.y;
        w = r2.w;
        h = r2.h;

        return *this;
    }

    bool Rect::operator== (const Rect& r2) const
    {
        return ((w == r2.w) && (h == r2.h));
    }

    bool Rect::operator!= (const Rect& r2) const
    {
        return !((w == r2.w) && (h == r2.h));
    }

}
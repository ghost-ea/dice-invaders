#ifndef __DiceInvaders__Size__hpp__
#define __DiceInvaders__Size__hpp__

namespace ea
{
    struct Size
    {
        Size() : width(0), height(0) {}
        Size(int width, int height) : width(width), height(height) {}
        int width = 0;
        int height = 0;

        Size& operator= (const Size& s2)
        {
            if (this == &s2)
                return *this;

            width = s2.width;
            height = s2.height;

            return *this;
        };

        Size& operator+= (const Size& s2)
        {
            width += s2.width;
            height += s2.height;

            return *this;
        };

        Size& operator-= (const Size& s2)
        {
            width -= s2.width;
            height -= s2.height;

            return *this;
        };

        Size& operator*= (const int scalar)
        {
            width *= scalar;
            height *= scalar;

            return *this;
        };

        Size& operator*= (const float fscalar)
        {
            width = (int)((float)width * fscalar);
            height = (int)((float)height * fscalar);

            return *this;
        };

        Size& operator/= (const int scalar)
        {
            width /= scalar;
            height /= scalar;

            return *this;
        };

        const Size operator+(const Size &s2) const
        {
            return Size(*this) += s2;
        };

        const Size operator-(const Size &s2) const
        {
            return Size(*this) -= s2;
        };

        const Size operator*(const int scalar) const
        {
            return Size(*this) *= scalar;
        };

        const Size operator*(const float fscalar) const
        {
            return Size(*this) *= fscalar;
        };

        const Size operator/(const int scalar) const
        {
            return Size(*this) /= scalar;
        };

        bool operator== (const Size& s2) const
        {
            return ((width == s2.width) && (height == s2.height));
        }

        bool operator!= (const Size& s2) const
        {
            return !((width == s2.width) && (height == s2.height));
        };
    };
}
#endif //defined(__DiceInvaders__Size__hpp__)
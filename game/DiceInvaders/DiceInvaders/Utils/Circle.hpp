#ifndef __DiceInvaders__Circle__hpp__
#define __DiceInvaders__Circle__hpp__

#include "Vector2D.hpp"
namespace ea
{
    struct Circle
    {
        Circle()
            : center({ 0.0f,0.0f })
            , radius(1.0f) {}

        Circle(Vector2D center, float radius)
            : center(center)
            , radius(radius) {}

        static float distance(const Circle& c1, const Circle& c2);
        static double distanceSquared(const Circle& v1, const Circle& v2);

        const Vector2D& getCenter() const
        {
            return center;
        }
        void setCenter(const Vector2D& c)
        {
            center = c;
        }

        float getRadius()
        {
            return radius;
        }

        void setRadius(float r)
        {
            radius = r;
        }

    protected:
        Vector2D center;
        float radius;
    };
}

#endif //defined(__DiceInvaders__Circle__hpp__)
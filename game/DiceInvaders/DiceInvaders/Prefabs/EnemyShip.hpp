#ifndef __DiceInvaders__EnemyShip__hpp__
#define __DiceInvaders__EnemyShip__hpp__

#include "IWeapon.hpp"
#include "IWarShip.hpp"

#include "GameObjectFactory.fwd.hpp"
#include "RenderComponent.fwd.hpp"
#include "CollisionComponent.fwd.hpp"
#include "Types.hpp"

namespace ea
{
    class EnemyShip : public IWarShip
    {
        friend class GameObjectFactory;

        const std::uniform_real_distribution<float> kBombChance{ 1, 15 };

    public:
        EnemyShip();
        ~EnemyShip();

        void init() override;
        void deinit() override;
        void start() override;
        void update(float deltaTime) override;
        void setActive(bool isActive) override;

        void setSpeed(const Vector2D& newSpeed);
        void flipSpeed();

    private:
        Vector2D _speed;
        void onCollisionEnter(const CollisionComponent& other);

        float _cooldownTimer = 0.0f;
        float _nextFireTrigger = 0.0f;

        void updateBomb();

        std::uniform_real_distribution<float> getBombChanceDistribution();


    };
}

#endif //defined(__DiceInvaders__EnemyShip__hpp__)
#include "EnemyShip.hpp"

#include "IWarDirector.hpp"
#include "GameManager.hpp"
#include "RenderComponent.hpp"
#include "ServiceLocator.hpp"
#include "CollisionComponent.hpp"
#include "Types.hpp"

#ifdef _DEBUG
#include "Logger.hpp"
#endif

namespace ea
{
    EnemyShip::EnemyShip()
    {
    }

    EnemyShip::~EnemyShip()
    {
    }

    void EnemyShip::init()
    {
        _renderer = new RenderComponent();
        _collider = new CollisionComponent();

        _collisionCallback = std::bind(&EnemyShip::onCollisionEnter, this, std::placeholders::_1);
    }

    void EnemyShip::start()
    {
        _cooldownTimer = 0.0f;
        _nextFireTrigger = GameRandom::getRandomFloat(getBombChanceDistribution());
    }

    void EnemyShip::update(float deltaTime)
    {
        if (!_isActive) { return; }

        _cooldownTimer += deltaTime;

        // check max position
        auto& game = ServiceLocator::get().getGameManager();
        auto worldSize = game.getWorld().size;

        float offset = _renderer->getOffset().width;
        float limitX = worldSize.width - offset;

        if (_position.x > limitX && _warDirector)
        {
            _warDirector->onWorldEdgeTouched(WarEdge::Right);
        }
        else if (_position.x < offset)
        {
            _warDirector->onWorldEdgeTouched(WarEdge::Left);
        }
        else if (_position.y > worldSize.height - offset)
        {
            // reached the bottom of the screen
            _warDirector->onWorldEdgeTouched(WarEdge::Bottom);
        }

        updateBomb();
    }

    void EnemyShip::updateBomb()
    {
        if (!_weapon)
        {
            return;
        }

        if (_cooldownTimer > _nextFireTrigger)
        {
            // randomly shoot without permission if I get a "six" killer
            if (GameRandom::getRandomDice() == 6)
            {
                _weapon->shoot(_position);
            }

            else if (_warDirector->requestPermissionToBomb(_position))
            {
                _weapon->shoot(_position);
            }

            _nextFireTrigger = GameRandom::getRandomFloat(getBombChanceDistribution());
            _cooldownTimer = 0.0f;
        }

    }

    void EnemyShip::setActive(bool isActive)
    {
        IGameObject::setActive(isActive);
        _renderer->setEnabled(isActive);
        _collider->setEnabled(isActive);
    }

    void EnemyShip::onCollisionEnter(const CollisionComponent& other)
    {
#ifdef _DEBUG
        std::string debugLog("[Enemy] Collision with " + other.getGameObject().getName() + "\n");
        DBOUT(debugLog.c_str());
#endif

        if (other.getLayerTag() == CollisionLayerTag::Rocket ||
            other.getLayerTag() == CollisionLayerTag::Player)
        {
            setActive(false);
            _warDirector->onEnemyShipDestroyed();
        }
    }

    void EnemyShip::setSpeed(const Vector2D& newSpeed)
    {
        _speed = newSpeed;
    }

    void EnemyShip::flipSpeed()
    {
        _speed.x = -_speed.x;
    }

    void EnemyShip::deinit()
    {
        SAFE_DELETE(_renderer);
        SAFE_DELETE(_collider);
    }

    std::uniform_real_distribution<float> EnemyShip::getBombChanceDistribution()
    {
        return kBombChance;
    }


}

#include "Bomb.hpp"

#include "RenderComponent.hpp"
#include "CollisionComponent.hpp"
#include "CollisionManager.hpp"
#include "ServiceLocator.hpp"
#include "GameManager.hpp"
#include "Types.hpp"

namespace ea
{
    Bomb::Bomb()
    {
    }

    Bomb::~Bomb()
    {
    }

    void Bomb::init()
    {
        _renderer = new RenderComponent();
        _collider = new CollisionComponent();

        setActive(false);
        _collisionCallback = std::bind(&Bomb::onCollisionEnter, this, std::placeholders::_1);
    }

    void Bomb::start()
    {
        setActive(awakeOnStart);
    }

    void Bomb::setActive(bool isActive)
    {
        IGameObject::setActive(isActive);
        _renderer->setEnabled(isActive);
        _collider->setEnabled(isActive);
    }

    void Bomb::update(float deltaTime)
    {
        if (!_isActive) { return; }

        auto& world = ServiceLocator::get().getGameManager().getWorld();

        if (_position.y > (world.origin.y + world.size.height))
        {
            setActive(false);
            return;
        }

        _position += _speed * deltaTime;// Vector2D(0.0f, _speed.y * deltaTime);
    }

    void Bomb::setSpeed(Vector2D speed)
    {
        _speed = speed;
    }

    void Bomb::onCollisionEnter(const CollisionComponent& other)
    {
        if (other.getLayerTag() == CollisionLayerTag::Player)
        {
            setActive(false);
            // apply damage...
        }
    }

    void Bomb::deinit()
    {
        SAFE_DELETE(_renderer);
        SAFE_DELETE(_collider);
    }
}

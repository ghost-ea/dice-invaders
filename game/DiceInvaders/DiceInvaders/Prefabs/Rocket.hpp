#ifndef __DiceInvaders__Rocket__hpp__
#define __DiceInvaders__Rocket__hpp__

#include "Rocket.fwd.hpp"

#include "IAmmo.hpp"
#include "GameObjectFactory.fwd.hpp"
#include "RenderComponent.fwd.hpp"
#include "CollisionComponent.fwd.hpp"
#include "Types.hpp"

namespace ea
{
    class Rocket : public IAmmo
    {
        friend class GameObjectFactory;

    public:
        Rocket();
        ~Rocket();

        void init() override;
        void deinit() override;
        void start() override;
        void update(float deltaTime) override;
        void setActive(bool isActive);

        void setSpeed(Vector2D speed) override;

    private:
        void onCollisionEnter(const CollisionComponent& other);
    };
}
#endif //defined(__DiceInvaders__Rocket__hpp__)

#include "RocketPool.hpp"

#include "Rocket.hpp"

namespace ea
{
    RocketPool::RocketPool()
    {
    }

    RocketPool::~RocketPool()
    {
    }

    void RocketPool::init()
    {
        _rockets.reserve(10);
    }

    void RocketPool::start()
    {

    }

    void RocketPool::update(float deltaTime)
    {

    }

    void RocketPool::loadAmmo(std::unique_ptr<IAmmo> ammo)
    {
        _rockets.emplace_back(std::move(ammo));
        _totalRockets = _rockets.size();
    }


    void RocketPool::shoot(Vector2D origin)
    {
        spawnRocket(origin, { 0.0f, -750.0f });
    }

    void RocketPool::spawnRocket(Vector2D position, Vector2D speed)
    {
        auto& rocket = _rockets[_nextRocket];
        rocket->setPosition(position);
        rocket->setSpeed(speed);
        rocket->setActive(true);
        _nextRocket = ++_nextRocket >= _totalRockets ? 0 : _nextRocket;
    }
}

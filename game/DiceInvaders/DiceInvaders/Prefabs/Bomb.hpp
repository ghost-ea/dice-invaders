#ifndef __DiceInvaders__Bomb__hpp__
#define __DiceInvaders__Bomb__hpp__

#include "IAmmo.hpp"

#include "Types.hpp"
#include "GameObjectFactory.fwd.hpp"
#include "RenderComponent.fwd.hpp"
#include "CollisionComponent.fwd.hpp"

namespace ea
{
    class Bomb : public IAmmo
    {
        friend class GameObjectFactory;

    public:
        Bomb();
        ~Bomb();

        void init() override;
        void deinit() override;
        void start() override;
        void update(float deltaTime) override;
        void setActive(bool isActive);

        void setSpeed(Vector2D speed) override;

    private:
        void onCollisionEnter(const CollisionComponent& other);
    };
}

#endif //defined(__DiceInvaders__Bomb__hpp__)


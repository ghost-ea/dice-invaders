#ifndef __DiceInvaders__WarDirector__hpp__
#define __DiceInvaders__WarDirector__hpp__

#include "WarDirector.fwd.hpp"

#include "IWarDirector.hpp"
#include "IWarShip.hpp"
#include "GameObjectFactory.fwd.hpp"
#include "Types.hpp"

namespace ea
{
    class WarDirector : public IWarDirector
    {
        friend class GameObjectFactory;

        const int kHorSpacing = 16;
        const int kVertSpacing = 48;
        const float kBaseStep = 20.0f;
        const int kMaxBombsPerTimeFrame = 5;
        const std::uniform_real_distribution<float> kSleepInterval{ 1, 5 };
        const float kMinBombDistance = 256.0f;

        const Vector2D kGoRight = { kBaseStep, 0.0f };
        const Vector2D kGoLeft = { -kBaseStep, 0.0f };
        const Vector2D kGoDownLeft = { -kBaseStep, kBaseStep };
        const Vector2D kGoDownRight = { kBaseStep, kBaseStep };

        enum class MovementState
        {
            MovingHorizontal = 0,
            CompletingRowUpdate,
            MovingDown,
        };

    public:
        WarDirector();
        ~WarDirector();

        void init() override;
        void deinit() override;
        void start() override;
        void update(float deltaTime) override;

        void onWorldEdgeTouched(WarEdge edge);
        void onEnemyShipDestroyed();
        bool requestPermissionToBomb(const Vector2D& position) override;
        void onPlayerTakeDamage(int livesRemaining) override;

    private:

        std::size_t _enemiesAlive;
        // Margins betweens each ship and between rows
        int _horSpacing = kHorSpacing;
        int _vertSpacing = kVertSpacing;

        Vector2D _speed;

        float _cooldownTimer = 0.0f;
        float _nextFireTrigger = 0.0f;

        std::size_t _lastUpdatedRow = 0;
        MovementState _state = MovementState::MovingHorizontal;
        WarEdge _lastEdgeTouched = WarEdge::None;
        bool _edgeWasTouched = false;
        int _bombsCounter = 0;
        float _bombsSleepingCounter = 0.0f;
        float _bombsPause = 0.0f;
        Vector2D _lastBombPosition = { 0.0f, 0.0f };

        Vector2D _direction = { 4.0f, 0.0f };
        Vector2D _movementPause = { 0.25f, 0.25f };
        Vector2D _movementSpeed = { 1.0f, 1.0f };

        void calculateSpeed();
        void updateTimer(float deltaTime);
        void resetTimer();
        bool timeToUpdate();
        void calculateState();
        void applyState();
        void applyHorizontalStep(Vector2D direction);
        void applyVerticalStep(Vector2D direction);


    };
}

#endif //defined(__DiceInvaders__WarDirector__hpp__)
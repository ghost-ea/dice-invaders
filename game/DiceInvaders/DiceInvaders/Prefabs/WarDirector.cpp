#include "WarDirector.hpp"

#include "IWarShip.hpp"
#include "ServiceLocator.hpp"
#include "LevelManager.hpp"
#include "RenderComponent.hpp"

namespace ea
{
    WarDirector::WarDirector()
    {
    }


    WarDirector::~WarDirector()
    {
    }

    void WarDirector::init()
    {
        int rows = _enemyFormationSize.height;
        int cols = _enemyFormationSize.width;

        std::size_t idx = _enemies.size() - 1;
        for (auto& enemy : _enemies)
        {
            enemy->setWarDirector(this);
            enemy->loadWeapon(_enemyBombs);
            enemy->setPosition({ (float)(_horSpacing + ((int)idx % cols) * _vertSpacing) ,(float)(_horSpacing + ((int)idx / cols) * _vertSpacing) });
            enemy->setActive(true);
            --idx;
        }

        _enemiesAlive = _enemies.size();

        resetTimer();
        _score.score = 0;
        _cooldownTimer = 0.0f;
        _bombsSleepingCounter = 0.0f;
        _bombsPause = 0.0f;
        _edgeWasTouched = false;
        _lastEdgeTouched = WarEdge::None;
        _bombsCounter = 0;
        _lastBombPosition = { 0.0f, 0.0f };
        _direction = { 4.0f, 0.0f };
        _movementPause = { 0.25f, 0.25f };
        _movementSpeed = { 1.0f, 1.0f };
    }

    void WarDirector::start()
    {
        _lastUpdatedRow = 0;
    }

    void WarDirector::update(float deltaTime)
    {
        if (!_isActive) { return; }

        auto& level = ServiceLocator::get().getLevelManager();

        if (_enemiesAlive == 0)
        {
            level.playerWon = true;
            return;
        }

        updateTimer(deltaTime);

        // if it's time to update
        if (timeToUpdate())
        {
            calculateState();
            applyState();
            resetTimer();
        }

        calculateSpeed();

        level.updateScore(_score);
    }

    void WarDirector::updateTimer(float deltaTime)
    {
        _cooldownTimer += deltaTime * _movementSpeed.x;
        _bombsSleepingCounter += deltaTime;
    }

    void WarDirector::resetTimer()
    {
        _cooldownTimer = 0.0f;
    }

    bool WarDirector::timeToUpdate()
    {
        return _cooldownTimer > _movementPause.x;
    }

    void WarDirector::calculateSpeed()
    {
        if (_enemiesAlive < _enemies.size() * 0.05f)
        {
            _movementSpeed = { 24.0f, 24.0f };
        }
        else if (_enemiesAlive < _enemies.size() * 0.1f)
        {
            _movementSpeed = { 8.0f, 8.0f };
        }
        else if (_enemiesAlive < _enemies.size() * 0.25f)
        {
            _movementSpeed = { 4.0f, 4.0f };
        }
        else if (_enemiesAlive < _enemies.size() * 0.50f)
        {
            _movementSpeed = { 2.0f, 2.0f };
        }
    }

    void WarDirector::calculateState()
    {
        switch (_state)
        {
            case MovementState::MovingHorizontal:
            {
                if (!_edgeWasTouched)
                {
                    return;
                }
                else
                {
                    _state = MovementState::CompletingRowUpdate;
                }
                break;
            }
            case MovementState::CompletingRowUpdate:
            {
                if (_lastUpdatedRow < _enemyFormationSize.height - 1)
                {
                    return;
                }
                else
                {
                    _state = MovementState::MovingDown;
                    _edgeWasTouched = false;
                }
                break;
            }
            case MovementState::MovingDown:
            {
                if (_lastUpdatedRow == _enemyFormationSize.height - 1)
                {
                    _state = MovementState::MovingHorizontal;
                }
                break;
            }
        }
    }

    void WarDirector::applyState()
    {
        switch (_state)
        {
            case MovementState::MovingHorizontal:
            {
                Vector2D direction = _lastEdgeTouched == WarEdge::Right ? kGoLeft : kGoRight;
                applyHorizontalStep(direction);
                break;
            }
            case MovementState::CompletingRowUpdate:
            {
                Vector2D direction = _lastEdgeTouched == WarEdge::Right ? kGoRight : kGoLeft;
                applyHorizontalStep(direction);
                break;
            }
            case MovementState::MovingDown:
            {
                Vector2D direction = _lastEdgeTouched == WarEdge::Right ? kGoDownLeft : kGoDownRight;
                applyVerticalStep(direction);
                break;
            }
        }

        _lastUpdatedRow = ++_lastUpdatedRow >= _enemyFormationSize.height ? 0 : _lastUpdatedRow;
    }

    void WarDirector::applyHorizontalStep(Vector2D direction)
    {
        for (std::size_t i = _lastUpdatedRow * _enemyFormationSize.width; i < (_lastUpdatedRow + 1) * _enemyFormationSize.width; ++i)
        {
            _enemies.at(i)->translateX(direction.x);
        }
    }

    void WarDirector::applyVerticalStep(Vector2D direction)
    {
        for (std::size_t i = _lastUpdatedRow * _enemyFormationSize.width; i < (_lastUpdatedRow + 1) * _enemyFormationSize.width; ++i)
        {
            _enemies.at(i)->translate(direction);
        }
    }

    void WarDirector::deinit()
    {
    }

    void WarDirector::onWorldEdgeTouched(WarEdge edge)
    {
        if (edge == WarEdge::Bottom)
        {
            auto& level = ServiceLocator::get().getLevelManager();
            level.updateModel(PlayerModel(0));

            return;
        }

        if (edge != _lastEdgeTouched)
        {
            _lastEdgeTouched = edge;
            _edgeWasTouched = true;
        }
    }

    void WarDirector::onEnemyShipDestroyed()
    {
        _enemiesAlive--;
        _score.score += 100;
        // call game manager to update points and model
    }

    bool WarDirector::requestPermissionToBomb(const Vector2D& position)
    {
        // if bombs system is sleeping
        if (_bombsSleepingCounter < _bombsPause)
        {
            return false;
        }

        // check the number of bombs dropped in the last window frame
        if (_bombsCounter >= kMaxBombsPerTimeFrame)
        {
            _bombsSleepingCounter = 0.0f;
            _bombsPause = GameRandom::getRandomFloat(kSleepInterval); // sleep a random time interval
            _bombsCounter = 0;
            return false;
        }

        if (position.x == _lastBombPosition.x)
        {
            return false;
        }

        // check if the bomb is too near to the previous one
        float distanceFromLast = (float)Vector2D::distance(position, _lastBombPosition);
        if (distanceFromLast < kMinBombDistance)
        {
            return false;
        }

        _lastBombPosition = position;
        ++_bombsCounter;
        return true;
    }


    void WarDirector::onPlayerTakeDamage(int livesRemaining)
    {
        // call game manager to update player model
    }

}


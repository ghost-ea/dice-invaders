#ifndef __DiceInvaders__BombPool__hpp__
#define __DiceInvaders__BombPool__hpp__

#include "BombPool.fwd.hpp"

#include "IGameObject.hpp"
#include "IWeapon.hpp"
#include "IAmmo.hpp"
#include "GameObjectFactory.fwd.hpp"
#include "Bomb.fwd.hpp"
#include "Types.hpp"

namespace ea
{
    class BombPool : public IWeapon
    {
        friend class GameObjectFactory;

    public:
        typedef std::vector<std::unique_ptr<IAmmo>> VBombs;

        BombPool();
        ~BombPool();

        void init() override;
        void start() override;
        void update(float deltaTime) override;

        void shoot(Vector2D origin) override;
        void dropBomb(Vector2D position, Vector2D speed);

        void loadAmmo(std::unique_ptr<IAmmo> ammo) override;

    private:
        std::size_t _totalBombs = 0;
        std::size_t _nextBomb = 0;
        VBombs _bombs;
    };
}
#endif //defined(__DiceInvaders__BombPool__hpp__)

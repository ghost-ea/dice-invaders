#include "BombPool.hpp"

#include "Bomb.hpp"

namespace ea
{
    BombPool::BombPool()
    {
    }

    BombPool::~BombPool()
    {
    }

    void BombPool::init()
    {
        _bombs.reserve(30);
    }

    void BombPool::start()
    {

    }

    void BombPool::update(float deltaTime)
    {

    }

    void BombPool::loadAmmo(std::unique_ptr<IAmmo> ammo)
    {
        _bombs.emplace_back(std::move(ammo));
        _totalBombs = _bombs.size();
    }

    void BombPool::shoot(Vector2D origin)
    {
        dropBomb(origin, { 0.0f, 500.0f });
    }

    void BombPool::dropBomb(Vector2D position, Vector2D speed)
    {
        auto& bomb = _bombs[_nextBomb];
        bomb->setPosition(position);
        bomb->setSpeed(speed);
        bomb->setActive(true);
        _nextBomb = ++_nextBomb >= _totalBombs ? 0 : _nextBomb;
    }
}

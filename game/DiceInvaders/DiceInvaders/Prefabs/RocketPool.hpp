#ifndef __DiceInvaders__RocketPool__hpp__
#define __DiceInvaders__RocketPool__hpp__

#include "RocketPool.fwd.hpp"

#include "IGameObject.hpp"
#include "IWeapon.hpp"
#include "IAmmo.hpp"
#include "GameObjectFactory.fwd.hpp"
#include "Rocket.fwd.hpp"
#include "Types.hpp"

namespace ea
{
    class RocketPool : public IWeapon
    {
        friend class GameObjectFactory;

    public:
        typedef std::vector<std::unique_ptr<IAmmo>> VRockets;

        RocketPool();
        ~RocketPool();

        void init() override;
        void start() override;
        void update(float deltaTime) override;

        void shoot(Vector2D origin) override;
        void spawnRocket(Vector2D position, Vector2D speed);

        void loadAmmo(std::unique_ptr<IAmmo> ammo) override;

    private:
        std::size_t _totalRockets = 0;
        std::size_t _nextRocket = 0;
        VRockets _rockets;
    };
}
#endif //defined(__DiceInvaders__RocketPool__hpp__)

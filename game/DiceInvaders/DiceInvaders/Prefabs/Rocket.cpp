#include "Rocket.hpp"

#include "RenderComponent.hpp"
#include "CollisionComponent.hpp"
#include "CollisionManager.hpp"
#include "ServiceLocator.hpp"
#include "GameManager.hpp"
#include "Types.hpp"

#ifdef _DEBUG
#include "Logger.hpp"
#endif

namespace ea
{
    Rocket::Rocket()
    {
    }

    Rocket::~Rocket()
    {
    }

    void Rocket::init()
    {
        _renderer = new RenderComponent();
        _collider = new CollisionComponent();

        setActive(false);
        _collisionCallback = std::bind(&Rocket::onCollisionEnter, this, std::placeholders::_1);
    }

    void Rocket::start()
    {
        setActive(awakeOnStart);
    }

    void Rocket::update(float deltaTime)
    {
        if (!_isActive) { return; }

        auto& world = ServiceLocator::get().getGameManager().getWorld();
        if (_position.y < world.origin.y)
        {
            setActive(false);
            return;
        }

        _position += _speed * deltaTime;//set a negative speed for coherence
    }

    void Rocket::setActive(bool isActive)
    {
        IGameObject::setActive(isActive);
        _renderer->setEnabled(isActive);
        _collider->setEnabled(isActive);
    }

    void Rocket::setSpeed(Vector2D speed)
    {
        _speed = speed;
    }

    void Rocket::onCollisionEnter(const CollisionComponent& other)
    {
#ifdef _DEBUG
        std::string debugLog("[Rocket] Collision with " + other.getGameObject().getName() + "\n");
        DBOUT(debugLog.c_str());
#endif
        if (other.getLayerTag() == CollisionLayerTag::Enemy)
        {
            setActive(false);
            // apply damage...
        }
    }

    void Rocket::deinit()
    {
        SAFE_DELETE(_renderer);
        SAFE_DELETE(_collider);
    }
}


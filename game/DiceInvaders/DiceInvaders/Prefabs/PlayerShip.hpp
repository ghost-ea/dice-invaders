#ifndef __DiceInvaders__PlayerShip__hpp__
#define __DiceInvaders__PlayerShip__hpp__

#include "IGameObject.hpp"

#include "Types.hpp"
#include "GameObjectFactory.fwd.hpp"
#include "RenderComponent.fwd.hpp"
#include "CollisionComponent.fwd.hpp"
#include "IWeapon.hpp"
#include "IWarShip.hpp"
#include "PlayerModel.hpp"

namespace ea
{
    class PlayerShip : public IWarShip
    {
        friend class GameObjectFactory;

        const int kMaxLives = 3;
        const float kBleedingTime = 2.0f;
        const float kBleedingFrequency = 0.25f;

    public:
        PlayerShip();
        ~PlayerShip();

        void setActive(bool isActive) override;
        void init() override;
        void start() override;
        void update(float deltaTime) override;

        float speed = 260.0f;
        float fireRatio = 0.5f;
        int life = kMaxLives;
        bool isBleeding = false;
        float bleedingCounter = 0.0f;
        float bleedingFrameCounter = 0.0f;

    private:
        float _lastFireTime = 0.0f;
        PlayerModel _model;

        void onCollisionEnter(const CollisionComponent& other);
        void captureInput(float deltaTime);
        void limitMovement();
        void debounceFireButton(float currentTime);
        void updateBleedingAnimation(float deltaTime);
        void onBleedingEnd();
        void startBleeding();
    };
}

#endif //defined(__DiceInvaders__PlayerShip__hpp__)

#include "PlayerShip.hpp"

#include "ServiceLocator.hpp"
#include "GameManager.hpp"
#include "EngineManager.hpp"
#include "LevelManager.hpp"

#include "RenderComponent.hpp"
#include "CollisionComponent.hpp"
#include "ServiceLocator.hpp"
#include "RocketPool.hpp"

#ifdef _DEBUG
#include "Logger.hpp"
#endif

namespace ea
{
    PlayerShip::PlayerShip()
    {
    }

    PlayerShip::~PlayerShip()
    {
    }

    void PlayerShip::init()
    {
        _renderer = new RenderComponent();
        _collider = new CollisionComponent();

        _collisionCallback = std::bind(&PlayerShip::onCollisionEnter, this, std::placeholders::_1);
    }

    void PlayerShip::setActive(bool isActive)
    {
        IGameObject::setActive(isActive);
        _renderer->setEnabled(isActive);
        _collider->setEnabled(isActive);
    }

    void PlayerShip::start()
    {
        auto& game = ServiceLocator::get().getGameManager();
        setActive(awakeOnStart);

        _model.lives = kDefaultLives;
        life = _model.lives;

        setPosition({ game.getWorld().size.width * 0.5f, game.getWorld().size.height - (float)_renderer->getSize().width });
        isBleeding = false;
        bleedingCounter = bleedingFrameCounter = 0.0f;
    }

    void PlayerShip::update(float deltaTime)
    {
        auto& engine = ServiceLocator::get().getEngineManager();

        auto& level = ServiceLocator::get().getLevelManager();
        float currentTime = engine.getTime();

        // if player is bleeding block controls for a second
        if (!isBleeding)
        {
            captureInput(deltaTime);
            limitMovement();
            debounceFireButton(currentTime);
        }
        else
        {
            updateBleedingAnimation(deltaTime);
        }

        level.updateModel(_model);


    }

    void PlayerShip::captureInput(float deltaTime)
    {
        auto& engine = ServiceLocator::get().getEngineManager();

        if (engine.getKeyPressed(KeyCode::Left))
        {
            _position.x -= (speed*deltaTime);
        }
        if (engine.getKeyPressed(KeyCode::Right))
        {
            _position.x += (speed*deltaTime);
        }
    }

    void PlayerShip::debounceFireButton(float currentTime)
    {
        auto& engine = ServiceLocator::get().getEngineManager();
        // debounce fire input adding a cooldown based on fire ratio of the ship
        bool isCoolingDown = (_lastFireTime + fireRatio) >= currentTime;
        if (engine.getKeyPressed(KeyCode::Fire) && !isCoolingDown)
        {
            if (_weapon)
            {
                _weapon->shoot(_position);
            }

            _lastFireTime = currentTime;
        }
    }

    void PlayerShip::limitMovement()
    {
        auto& game = ServiceLocator::get().getGameManager();
        auto worldSize = game.getWorld().size;

        float offset = _renderer->getOffset().width;
        float limitX = worldSize.width - offset;

        if (_position.x > limitX)
        {
            _position.x = limitX;
        }
        else if (_position.x < offset)
        {
            _position.x = offset;
        }
    }

    void PlayerShip::onCollisionEnter(const CollisionComponent& other)
    {
#ifdef _DEBUG
        std::string debugLog("[Player] Collision with " + other.getGameObject().getName() + "\n");
        DBOUT(debugLog.c_str());
#endif

        if (other.getLayerTag() == CollisionLayerTag::Bomb ||
            other.getLayerTag() == CollisionLayerTag::Enemy)
        {
            startBleeding();
        }
    }

    void PlayerShip::startBleeding()
    {
        if (isBleeding)
        {
            return;
        }

        isBleeding = true;
        _collider->setEnabled(false);
        life--;

        _model.lives = life;
    }

    void PlayerShip::updateBleedingAnimation(float deltaTime)
    {
        bleedingCounter += deltaTime;
        bleedingFrameCounter += deltaTime;

        // switch on and off the renderer
        if (bleedingFrameCounter > kBleedingFrequency)
        {
            _renderer->setEnabled(!_renderer->enabled());
            bleedingFrameCounter = 0.0f;
        }

        if (bleedingCounter > kBleedingTime)
        {
            onBleedingEnd();
        }
    }

    void PlayerShip::onBleedingEnd()
    {
        bleedingCounter = 0.0f;
        bleedingFrameCounter = 0.0f;
        _renderer->setEnabled(true);
        _collider->setEnabled(true);
        isBleeding = false;
    }

}

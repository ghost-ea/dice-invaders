#ifndef __DiceInvaders__IAmmo__hpp__
#define __DiceInvaders__IAmmo__hpp__

#include "Types.hpp"
#include "IGameObject.hpp"

namespace ea
{
    class IAmmo : public IGameObject
    {
        friend class GameManager;
        friend class GameObjectFactory;

    protected:
        IAmmo() {};

    public:
        virtual ~IAmmo() {};

        IAmmo(IAmmo const &) = delete;
        IAmmo & operator=(IAmmo const &) = delete;

        virtual void setSpeed(Vector2D speed) = 0;

    protected:
        Vector2D _speed = Vector2D::Zero();

    };
}

#endif //defined(__DiceInvaders__IAmmo__hpp__)
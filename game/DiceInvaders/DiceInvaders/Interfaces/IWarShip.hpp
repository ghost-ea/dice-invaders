#ifndef __DiceInvaders__IWarShip__hpp__
#define __DiceInvaders__IWarShip__hpp__

#include "Types.hpp"
#include "IGameObject.hpp"
#include "IWeapon.hpp" 
#include "IWarDirector.hpp"

namespace ea
{
    class IWarDirector;

    class IWarShip : public IGameObject
    {
        friend class GameManager;
        friend class GameObjectFactory;

    protected:
        IWarShip() {};

    public:
        virtual ~IWarShip() {};

        IWarShip(IWarShip const &) = delete;
        IWarShip & operator=(IWarShip const &) = delete;

        virtual void loadWeapon(IWeapon* weapon) { _weapon = weapon; };
        virtual void shootWeapon() {};
        virtual void setWarDirector(IWarDirector* warDirector) { _warDirector = warDirector; };

    protected:
        IWeapon* _weapon = nullptr;
        IWarDirector* _warDirector = nullptr;
    };
}

#endif //defined (__DiceInvaders__IWarShip__hpp__)

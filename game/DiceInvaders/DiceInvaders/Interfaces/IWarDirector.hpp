#ifndef __DiceInvaders__IWarDirector__hpp__
#define __DiceInvaders__IWarDirector__hpp__

#include "GameObjectFactory.fwd.hpp"

#include "IGameObject.hpp"
#include "IWeapon.hpp" 
#include "IWarShip.hpp"
#include "Types.hpp"
#include "ScoreModel.hpp"

namespace ea
{
    class IWarShip;

    enum class WarEdge
    {
        None = 0,
        Right,
        Left,
        Bottom,
    };

    class IWarDirector : public IGameObject
    {
        friend class GameManager;
        friend class GameObjectFactory;

        typedef std::unique_ptr<IWarShip> WarShip;
        typedef std::vector<WarShip> VWarShip;

    protected:
        IWarDirector() {};

    public:
        virtual ~IWarDirector() {};

        IWarDirector(IWarDirector const &) = delete;
        IWarDirector & operator=(IWarDirector const &) = delete;

        virtual void loadEnemyFormation(VWarShip formation) { _enemies = std::move(formation); };
        virtual void loadEnemyWeapon(IWeapon* weapon) { _enemyBombs = weapon; };

        virtual void onWorldEdgeTouched(WarEdge edge) {};
        virtual void onEnemyShipDestroyed() {};
        virtual bool requestPermissionToBomb(const Vector2D& position) { return true; };

        virtual void onPlayerTakeDamage(int livesRemaining) {};

    protected:
        VWarShip _enemies;
        Size _enemyFormationSize; // cols = Size.width,  rows = Size.height
        IWeapon* _enemyBombs;
        ScoreModel _score;

    };
}


#endif //define(__DiceInvaders__IWarDirector__hpp__)
#ifndef __DiceInvaders__IWeapon__hpp__
#define __DiceInvaders__IWeapon__hpp__

#include "Types.hpp"
#include "IGameObject.hpp"
#include "IAmmo.hpp" 

namespace ea
{
    class IWeapon : public IGameObject
    {
        friend class GameManager;
        friend class GameObjectFactory;

    protected:
        IWeapon() {};

    public:
        virtual ~IWeapon() {};

        IWeapon(IWeapon const &) = delete;
        IWeapon & operator=(IWeapon const &) = delete;

        virtual void shoot(Vector2D origin) = 0;

        virtual void loadAmmo(std::unique_ptr<IAmmo> ammo) {};

    };
}

#endif //defined(__DiceInvaders__IWeapon__hpp__)
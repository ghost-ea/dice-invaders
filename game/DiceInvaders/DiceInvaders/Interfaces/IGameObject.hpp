#ifndef __DiceInvaders__IGameObject__hpp__
#define __DiceInvaders__IGameObject__hpp__

#include "Types.hpp"

#include "RenderComponent.fwd.hpp"
#include "CollisionComponent.fwd.hpp"

namespace ea
{
    class IGameObject
    {
        friend class GameManager;
        friend class GameObjectFactory;
    public:

        virtual ~IGameObject() {};

        IGameObject(IGameObject const &) = delete;
        IGameObject & operator=(IGameObject const &) = delete;

    protected:
        IGameObject() {};

    public:
        bool awakeOnStart = true;

        virtual void init() {};
        virtual void deinit() {};
        virtual void start() {};
        virtual void update(float deltaTime) {};

        virtual void setActive(bool isActive) { _isActive = isActive; };

        const std::string& getName() const { return _name; }
        void setName(const std::string& name) { _name = name; }

        const Vector2D& getPosition() const { return _position; }
        void setPosition(Vector2D position) { _position = position; }

        void translate(const Vector2D& translation) { _position += translation; };
        void translateX(const float tX) { translate({ tX, 0.0f }); };
        void translateY(const float tY) { translate({ 0.0f, tY }); };

        virtual const OnCollisionDetected& getCollisionCallback() const { return _collisionCallback; };

    protected:
        bool _isActive = true;
        std::size_t _id = 0;
        std::string _name;
        Vector2D _position = { 0,0 };

        OnCollisionDetected _collisionCallback;
        RenderComponent* _renderer = nullptr;
        CollisionComponent* _collider = nullptr;

    protected:

    };
}

#endif //defined(__DiceInvaders__IGameObject__hpp__)


#ifndef __DiceInvaders__IComponent__hpp__
#define __DiceInvaders__IComponent__hpp__

namespace ea
{
    class IComponent
    {
        friend class IGameObject;
        friend class GameObjectFactory;

    public:
        virtual ~IComponent() {};

        IComponent(IComponent const &) = delete;
        IComponent& operator=(IComponent const &) = delete;

    protected:
        IComponent() {};

    public:
        virtual void init() {};
        virtual void start() {};
        virtual void update() {};

    protected:
        IGameObject* _gameObject = nullptr;
        int _id;
        bool _isEnabled = true;
    };
}

#endif
